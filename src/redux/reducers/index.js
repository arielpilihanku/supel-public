import { combineReducers } from "redux";

import areaReducer from './area';

export default combineReducers({areaReducer});