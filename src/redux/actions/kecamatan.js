import { INITIAL_KECAMATAN, PAYLOAD_KECAMATAN } from "../types/area";
import Api from "../../libraries/api";

export const payloadKecamatan = (kabupaten) => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_KECAMATAN,
            payload: {
                loading: true,
                data: false,
                errorMessage: false
            }
        });

        Api.get(`/kecamatan/dapil?kode_kabupaten=${kabupaten}`).then(resp => {

            if (resp) {
                dispatch({
                    type: PAYLOAD_KECAMATAN,
                    payload: {
                        loading: false,
                        data: resp,
                        errorMessage: false
                    }
                });
            }

        }).catch(err => {
            dispatch({
                type: PAYLOAD_KECAMATAN,
                payload: {
                    loading: false,
                    data: false,
                    errorMessage: err.message
                }
            });
        });
    }
}