import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Api from "../../../libraries/api";
import { toast, ToastContainer } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import { history } from "../../../shared/configure-store";

require('dotenv').config();

class Wilayah extends Component {
    _isMounted = false;

    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            orderBy: '',
            sortedBy: '',
            searchBy: '',

            roles: [],
            showDialog: false,
            idDelete: '',
            currDelete: '',

            rows: [],
            total: 0,
            perPage: 10,
            currentPage: 1,
            currentPageTable: 0,

            isEdit: false,
            isDelete: false,
            isCreate: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Halaman List Wilayah';

        this.__fetchData(false);

    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    __fetchData = update => {
        this._isMounted = true;

        let page = update ? parseInt(this.state.currentPage + 1) : this.state.currentPage;

        let sort = this.state.orderBy ? '&orderBy=' + this.state.orderBy + '&sortedBy=' + this.state.sortedBy : '';
        let search = this.state.searchBy ? '&search=' + this.state.searchBy : '';

        let route = '';

        if (search) {
            route = '/areas?limit=' + this.state.perPage + sort + search;
        } else {
            route = '/areas?limit=' + this.state.perPage + '&page=' + page + sort + search;
        }

        Api.get(route).then(resp => {
            if (this._isMounted) {

                if (resp) {

                    this.setState({
                        rows: Object.keys(resp).map(key => resp[key]),
                        loading: false,
                    })

                }
            }
        }).catch(err => {
            console.log(err);
        });
    };

    handleSearch = () => {
        this.__fetchData(false);
    };
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.__fetchData(false);
        }
    };

    handleSort = (val) => {

        let sortedBy = this.state.sortedBy;

        if (val !== this.state.orderBy) {
            sortedBy = 'asc';
        } else {
            if (sortedBy && sortedBy === 'asc') {
                sortedBy = 'desc';
            } else {
                sortedBy = 'asc';
            }
        }

        this.setState({
            orderBy: val,
            sortedBy: sortedBy,
            loading: true
        }, () => {
            this.__fetchData(false);
        });
    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };

    handleOpen = (row) => {
        this.setState({
            showDialog: true,
            idDelete: row.id,
            currDelete: row.name,
        })
    };

    handleClose = () => {
        this.setState({
            showDialog: false,
        })
    };

    handleDelete = () => {

        
    };

    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleChangePage = (event, newPage) => {
        this.setState({
            rows: [],
            currentPage: newPage,
            currentPageTable: newPage,
            loading: true,
        }, () => {
            this.__fetchData(true);
        });
    };

    handleChangeRowsPerPage = event => {
        this.setState({
            perPage: parseInt(event.target.value, 10),
            currentPage: 1,
            loading: true,
        }, () => {
            this.__fetchData(false);
        });
    };

    handleGo = (link) => {

        history.push(link);
    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Wilayah</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Wilayah</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <div className="row align-items-center mb-md-3">
                            <div className="col-md-6">
                                <TextField
                                    id="input-with-icon-textfield"
                                    variant="outlined"
                                    className="search-field"
                                    onChange={(e) => this.handleChange(e, 'searchBy')}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleSearch}
                                    placeholder="Cari disini"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="start">
                                                <IconButton
                                                    aria-label="Search click"
                                                    onClick={this.handleSearch}
                                                >
                                                    <i className="fas fa-search"> </i>
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                            <div className="col-md-6 text-md-right">

                            </div>
                        </div>

                        <Table className="table-list" size="small" >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Kode Provinsi</TableCell>
                                    <TableCell>Provinsi</TableCell>
                                    <TableCell>Kode Kabupaten</TableCell>
                                    <TableCell>Kabupaten</TableCell>
                                    <TableCell>Kode Kecamatan</TableCell>
                                    <TableCell>Kecamatan</TableCell>
                                    <TableCell>Kode Desa</TableCell>
                                    <TableCell>Desa</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.loading ? (
                                    <TableRow>
                                        <TableCell colSpan={6} align="center" className="py-5">
                                            <CircularProgress color="primary" />
                                        </TableCell>
                                    </TableRow>
                                ) : (
                                    this.state.rows.length === 0 ? (
                                        <TableRow style={{ height: 33 * this.state.perPage }}>
                                            <TableCell colSpan={5} style={{ textAlign: "center" }}>Belum ada peraturan disimpan</TableCell>
                                        </TableRow>
                                    ) : (
                                        this.state.rows.map(row => (
                                            <TableRow
                                                key={row.id}
                                                selected={row.selected}>
                                                <TableCell>
                                                    <span>{row.kode_provinsi === null ? '' : row.kode_provinsi}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.provinsi === null ? '' : row.provinsi}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kode_kabupaten === null ? '' : row.kode_kabupaten}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kabupaten === null ? '' : row.kabupaten}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kode_kecamatan === null ? '' : row.kode_kecamatan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kecamatan === null ? '' : row.kecamatan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kode_kelurahan === null ? '' : row.kode_kelurahan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kelurahan === null ? '' : row.kelurahan}</span>
                                                </TableCell>
                                            </TableRow>
                                        ))
                                    )
                                )}

                            </TableBody>
                        </Table>
                    </div>

                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default Wilayah;