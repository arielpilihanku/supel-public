import React from "react";
import Api from "../../../libraries/api";
import { toast, ToastContainer } from "react-toastify";
import Storage from "../../../libraries/storage";
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Avatar from '@material-ui/core/Avatar';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import LoadingContent from "../survey/loading";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";


require('dotenv').config();

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));


export default function TimsesMobile(props) {

    const classes = useStyles();

    const [loading, setLoading] = React.useState(false);
    const [isSearch, setIsSearch] = React.useState(false);
    const [timses, setTimses] = React.useState([]);
    const [search, setSearch] = React.useState('');

    React.useEffect(() => {

        const token = props.match.params.id;

        Storage.set('access_token', token);

        setLoading(true)

        Api.get('/timses').then(resp => {

            if (resp.data) {
                const data = resp.data;

                setTimses(data);

                setLoading(false)
            }

        }).catch(err => {
            console.log(err);
        });


    }, []);



    const searchTimses = async (value) => {

        setLoading(true);

        setIsSearch(true);

        Api.get('/timses?search=' + value).then(resp => {

            if (resp) {

                const data = resp.data;

                setTimses(data);

                setLoading(false);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const handleSubmit = async () => {


    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };


    const handleBack = () => {

    };

    const handleSearch = () => {
        searchTimses(search);
    };


    return (
        <div className="row main-content">

            <div className="col-md-12 mt-4 ml-3">
                <TextField
                    id="input-with-icon-textfield"
                    variant="outlined"
                    className=""
                    onChange={e => setSearch(e.target.value)}
                    placeholder="Cari disini"
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton
                                    aria-label="Search click"
                                    onClick={handleSearch}
                                >
                                    <i className="fas fa-search"> </i>
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
            </div>



            <List className="ml-3">
                {loading ? (
                    <LoadingContent />
                ) : (
                    timses.length === 0 ? (
                        <ListItem>
                            <ListItemIcon>
                                <NotInterestedIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary="Tidak Ada Data"
                                secondary=""
                            />
                        </ListItem>
                    ) : (
                        timses.map(option => (
                            <>
                                <ListItem alignItems="flex-start" key={option.id}>
                                    <ListItemAvatar>
                                        <Avatar alt={option.name} src="/static/images/avatar/1.jpg" />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={
                                            <>
                                                <span>{option.name}</span>
                                                <span className={option.status}>{option.status}</span>
                                            </>
                                        }
                                        secondary={
                                            <>
                                                <span>{option.saksi}</span>
                                            </>
                                        }
                                    />
                                </ListItem>
                                <Divider variant="inset" component="li" />
                            </>
                        ))
                    )
                )}
            </List>

            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">

                </div>
            </div>

            <ToastContainer />

        </div>
    )

}

