import React from "react";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import { toast, ToastContainer } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import MenuItem from '@material-ui/core/MenuItem';
import Storage from "../../../libraries/storage";
import { makeStyles, useTheme } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CircularProgress from '@material-ui/core/CircularProgress';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import ChartKandidat from "./chart";
import FilterVoters from "./filter-voters";
import FilterListIcon from '@material-ui/icons/FilterList';
import ClearIcon from '@material-ui/icons/Clear';


require('dotenv').config();

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '36ch',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
    active: {
        backgroundColor: "red"
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightBold,
    },
}));

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography component={'span'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

export default function SurveyAdd(props) {

    const classes = useStyles();
    const theme = useTheme();

    const [loading, setLoading] = React.useState(false);
    const [loadingSearch, setLoadingSearch] = React.useState(false);
    const [value, setValue] = React.useState(0);
    const [search, setSearch] = React.useState('');
    const [voters, setVoters] = React.useState([]);
    const [idVoter, setIdVoter] = React.useState('');
    const [animation, setAnimation] = React.useState('close');
    const [display, setDisplay] = React.useState(false);
    const [isSearch, setIsSearch] = React.useState(false);
    const [dataPresiden, setDataPresiden] = React.useState([]);
    const [dataGubernur, setDataGubernur] = React.useState([]);
    const [dataBupati, setDataBupati] = React.useState([]);
    const [dataDPR, setDataDPR] = React.useState([]);
    const [dataDPD, setDataDPD] = React.useState([]);
    const [dataDPRA, setDataDPRA] = React.useState([]);
    const [dataDPRK, setDataDPRK] = React.useState([]);
    const [status, setStatus] = React.useState([]);
    const [idPresiden, setIdPresiden] = React.useState('');
    const [idBupati, setIdBupati] = React.useState('');
    const [idGubernur, setIdGubernur] = React.useState('');
    const [idDPR, setIdDPR] = React.useState('');
    const [idDPD, setIdDPD] = React.useState('');
    const [idDPRA, setIdDPRA] = React.useState('');
    const [idDPRK, setIdDPRK] = React.useState('');
    const [partaiDPR, setPartaiDPR] = React.useState('');
    const [partaiDPD, setPartaiDPD] = React.useState('');
    const [partaiDPRA, setPartaiDPRA] = React.useState('');
    const [partaiDPRK, setPartaiDPRK] = React.useState('');
    const [statusPresiden, setStatusPresiden] = React.useState('');
    const [statusGubernur, setStatusGubernur] = React.useState('');
    const [statusBupati, setStatusBupati] = React.useState('');
    const [statusDPR, setStatusDPR] = React.useState('');
    const [statusDPD, setStatusDPD] = React.useState('');
    const [statusDPRA, setStatusDPRA] = React.useState('');
    const [statusDPRK, setStatusDPRK] = React.useState('');
    const [partai, setPartai] = React.useState([]);


    const [filterOpen, setFilterOpen] = React.useState(false);
    const [loadingFilter, setLoadingFilter] = React.useState(false);
    const [areaFilter, setAreaFilter] = React.useState([]);

    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');

    const [subCampaign, setSubCampaign] = React.useState('');
    const [pemilihan, setPemilihan] = React.useState('');


    const dataStatus = [
        {
            id: 'Pasti dipilih',
            name: "Pasti dipilih"
        },
        {
            id: 'Mungkin dipilih',
            name: "Mungkin dipilih"
        },
        {
            id: 'Tidak yakin',
            name: "Tidak yakin"
        }
    ];


    React.useEffect(() => {

        const token = props.match.params.id;

        Storage.set('access_token', token);

        setLoading(true);

        Api.get('/profile').then(resp => {

            if (resp.campaign) {
                const campaign = resp.campaign;
                const subCampaign = campaign.subgroup_campaign_id;
                setSubCampaign(parseInt(subCampaign));
                setPemilihan(campaign.pemilihan);
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/capres').then(resp => {

            if (resp.data) {
                const data = resp.data;

                setDataPresiden(data);
            }

        }).catch(err => {
            console.log(err);
        });


        fetchCampaign(1);
        fetchCampaign(2);
        fetchCampaign(3);
        fetchCampaign(4);
        fetchCampaign(5);
        fetchCampaign(6);

        Api.get('/partai?limit=0').then(resp => {

            if (resp.data) {
                const data = resp.data;

                setPartai(data);
            }

        }).catch(err => {
            console.log(err);
        });

        setStatus(dataStatus);


    }, []);


    const fetchCampaign = async (subGroup) => {

        Api.get('/campaigns?subgroup_campaign_id=' + subGroup).then(resp => {


            if (resp.data) {

                const data = resp.data;

                switch (subGroup) {
                    case 1:
                        setDataGubernur(data);
                        break;
                    case 2:
                        setDataBupati(data);
                        break;
                    case 3:
                        setDataDPR(data);
                        break;
                    case 4:
                        setDataDPD(data);
                        break;
                    case 5:
                        setDataDPRA(data);
                        break;
                    default:
                        setDataDPRK(data);
                }


            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPR = async (value) => {

        Api.get('/campaigns?subgroup_campaign_id=3&partai=' + value).then(resp => {

            if (resp.data) {

                const data = resp.data;

                setDataDPR(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPD = async (value) => {

        Api.get('/campaigns?subgroup_campaign_id=4&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPD(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPRA = async (value) => {

        Api.get('/campaigns?subgroup_campaign_id=5&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRA(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchCampaignDPRK = async (value) => {

        Api.get('/campaigns?subgroup_campaign_id=6&partai=' + value).then(resp => {


            if (resp.data) {

                const data = resp.data;

                setDataDPRK(data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchVoters = async () => {

        setLoading(true);

        let formData = new FormData();
        formData.append('cari', search);
        formData.append('kodeKabupaten', kodeKabupaten);
        formData.append('kodeKecamatan', kodeKecamatan);
        formData.append('kodeDesa', kodeDesa);

        Api.putFile('/search/voters', {
            method: 'POST',
            body: formData
        }).then(resp => {

            const data = resp.data;

            setVoters(data);

            setLoading(false);

            if (data.length === 0) {
                setIsSearch(true);
            }

        }).catch(err => {


            setLoading(false);

            showMessage(false, err.message);

        });

    };

    const handleSubmit = async () => {

        const params = [
            {
                'campaign_id': idPresiden,
                'status': statusPresiden
            },
            {
                'campaign_id': idGubernur,
                'status': statusGubernur
            },
            {
                'campaign_id': idBupati,
                'status': statusBupati
            },
            {
                'campaign_id': idDPR,
                'status': statusDPR
            },
            {
                'campaign_id': idDPD,
                'status': statusDPD
            },
            {
                'campaign_id': idDPRA,
                'status': statusDPRA
            },
            {
                'campaign_id': idDPRK,
                'status': statusDPRK
            }
        ];

        setLoading(true);

        let formData = new FormData();
        formData.append('survey', JSON.stringify(params));
        formData.append('voter_id', idVoter);

        Api.putFile('/voters/campaign', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoading(false);

            setAnimation('close')
            setDisplay(false)
            setValue(0);

            showMessage(true, 'Calon pemilih berhasil ditambahkan');

        }).catch(err => {


            setLoading(false);

            showMessage(false, err.message);

        });


    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };


    const handleChangeList = (event, newValue) => {
        setIdVoter(newValue);
        const tempVoters = voters.filter(data => data.id === newValue);

        setVoters(tempVoters);
        setAnimation('open')
        setDisplay(true)
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            fetchVoters();
        }
    };

    const handleSearch = () => {
        fetchVoters();
        setAnimation('close')
        setDisplay(false)
    };

    function handleChangePartai(e, prop) {

        const value = e.target.value;

        if (prop === 'dpr') {
            setPartaiDPR(value);
            fetchCampaignDPR(value);
        }

        if (prop === 'dpd') {
            setPartaiDPD(value);
            fetchCampaignDPD(value);
        }

        if (prop === 'dpra') {
            setPartaiDPRA(value);
            fetchCampaignDPRA(value);
        }

        if (prop === 'dprk') {
            setPartaiDPRK(value);
            fetchCampaignDPRK(value);
        }

    };

    const handleOpen = () => {
        setFilterOpen(true);
    };

    const handleClose = () => {
        setFilterOpen(false);
    };

    const handleClearFilter = () => {
        setAreaFilter([]);
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
    };

    const handleSubmitFilter = (kodeProvince, kodeKabupaten, kodeKecamatan, kodeDesa) => {
        setLoadingFilter(true);

        const kode = [kodeProvince];

        if (kodeKabupaten != '') {
            kode.push(kodeKabupaten);
            setKodeKabupaten(kodeKabupaten);
        }

        if (kodeKecamatan != '') {
            kode.push(kodeKecamatan);
            setKodeKecamatan(kodeKecamatan);
        }

        if (kodeDesa != '') {
            kode.push(kodeDesa);
            setKodeDesa(kodeDesa);
        }

        let formData = new FormData();
        kode.forEach(kd => formData.append('kode[]', kd))

        Api.putFile('/find/areas', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setAreaFilter(Object.keys(resp).map(key => resp[key]));

        }).catch(err => {
            console.log(err);
        });
        setLoadingFilter(false);
        setFilterOpen(false);

        fetchVoters();

    };


    return (
        <div className="row main-content">

            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Pendukung" {...a11yProps(0)} />
                    <Tab label="Tambah Pendukung" {...a11yProps(1)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <ChartKandidat />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <TextField
                        id="input-with-icon-textfield"
                        variant="outlined"
                        className="search-field"
                        onChange={e => setSearch(e.target.value)}
                        onKeyDown={handleKeyDown}
                        onBlur={handleSearch}
                        placeholder="Cari disini"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="start">
                                    <IconButton
                                        aria-label="Search click"
                                        onClick={handleSearch}
                                    >
                                        <i className="fas fa-search"> </i>
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />

                    <div className="inner-filter">
                        <Button className="filter-btn" onClick={handleOpen}>
                            <FilterListIcon />
                            Filter
                        </Button>
                        {areaFilter.map(option => (
                            <Button className="filter-title" key={option.id}>
                                {option.nama}
                            </Button>
                        ))}
                        {areaFilter.length > 1 && <Button className="clear-btn" onClick={handleClearFilter}>
                            <ClearIcon />
                            Clear
                        </Button>}
                    </div>

                    <List className={classes.root}>
                        {loadingSearch ? (
                            <div><CircularProgress color="primary" /></div>
                        ) : (
                            (voters.length === 0 && isSearch) ? (
                                <ListItem>
                                    <ListItemIcon>
                                        <NotInterestedIcon />
                                    </ListItemIcon>
                                    <ListItemText
                                        primary="Tidak Ada Data"
                                        secondary=""
                                    />
                                </ListItem>
                            ) : (
                                voters.map(option => (
                                    <>
                                        <ListItem
                                            key={option.id}
                                            alignItems="flex-start"
                                            selected={option.id == idVoter ? true : false}
                                            onClick={e => handleChangeList(e, option.id)}
                                            classes={{ selected: classes.active }}>
                                            <ListItemAvatar>
                                                <Avatar alt={option.name} src="/static/images/avatar/1.jpg" />
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={
                                                    <span className="block-area">
                                                        <span className="area">{option.nama_provinsi}</span>
                                                        <span className="area">{option.nama_kabupaten}</span>
                                                        <span className="area">{option.nama_kecamatan}</span>
                                                        <span className="area">{option.nama_desa}</span>
                                                    </span>
                                                }
                                                secondary={
                                                    <>
                                                        <span className="name">{option.name}</span><br />
                                                        <span className="case">{'Nik: ' + option.nik}</span><br />
                                                        <span className="case">{'Tempat: ' + option.tempat_lahir}</span><br />
                                                        <span className="case">{'Tgllahir: ' + option.tanggal_lahir}</span><br />
                                                        <span className="case">{'Alamat: ' + option.alamat}</span><br />
                                                    </>
                                                }
                                            />
                                        </ListItem>
                                        <Divider variant="inset" component="li" />
                                    </>
                                )))
                        )}
                    </List>

                    {display
                        ? <div className={`Modal ${animation}`}>
                            {pemilihan === 'Presiden' && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Presiden</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='presiden'
                                                    name='presiden'
                                                    label='Calon Presiden'
                                                    onChange={e => setIdPresiden(e.target.value)}
                                                    value={idPresiden}
                                                    fullWidth
                                                >
                                                    {dataPresiden.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusPresiden}
                                                value={statusPresiden}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusPresiden(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 1 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Gubernur</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='gubernur'
                                                    name='gubernur'
                                                    label='Calon Gubernur'
                                                    onChange={e => setIdGubernur(e.target.value)}
                                                    value={idGubernur}
                                                    fullWidth
                                                >
                                                    {dataGubernur.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusGubernur}
                                                value={statusGubernur}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusGubernur(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 2 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel3a-content"
                                    id="panel3a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>Bupati</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='bupati'
                                                    name='bupati'
                                                    label='Calon Bupati'
                                                    onChange={e => setIdBupati(e.target.value)}
                                                    value={idBupati}
                                                    fullWidth
                                                >
                                                    {dataBupati.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusBupati}
                                                value={statusBupati}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusBupati(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 3 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel4a-content"
                                    id="panel4a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR RI</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpr'
                                                    name='partai-dpr'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpr')}
                                                    value={partaiDPR}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpr'
                                                    name='dpr'
                                                    label='Calon DPR'
                                                    onChange={e => setIdDPR(e.target.value)}
                                                    value={idDPR}
                                                    fullWidth
                                                >
                                                    {dataDPR.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPR}
                                                value={statusDPR}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPR(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>

                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 4 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel5a-content"
                                    id="panel5a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPD</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpd'
                                                    name='partai-dpd'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpd')}
                                                    value={partaiDPD}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpd'
                                                    name='dpd'
                                                    label='Calon DPD'
                                                    onChange={e => setIdDPD(e.target.value)}
                                                    value={idDPD}
                                                    fullWidth
                                                >
                                                    {dataDPD.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPD}
                                                value={statusDPD}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPD(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 5 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel6a-content"
                                    id="panel6a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR TK I / DPRA</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dpra'
                                                    name='partai-dpra'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dpra')}
                                                    value={partaiDPRA}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dpra'
                                                    name='dpra'
                                                    label='Calon DPRA'
                                                    onChange={e => setIdDPRA(e.target.value)}
                                                    value={idDPRA}
                                                    fullWidth
                                                >
                                                    {dataDPRA.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPRA}
                                                value={statusDPRA}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPRA(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            { subCampaign === 6 && 
                            <Accordion>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel7a-content"
                                    id="panel7a-header"
                                >
                                    <Typography component={'span'} className={classes.heading}>DPR TK II / DPRK</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Partai</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='partai-dprk'
                                                    name='partai-dprk'
                                                    label='Partai'
                                                    onChange={e => handleChangePartai(e, 'dprk')}
                                                    value={partaiDPRK}
                                                    fullWidth
                                                >
                                                    {partai.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <label>Nama Kandidat</label>
                                                <TextField variant='outlined'
                                                    select
                                                    id='dprk'
                                                    name='dprk'
                                                    label='Calon DPRK'
                                                    onChange={e => setIdDPRK(e.target.value)}
                                                    value={idDPRK}
                                                    fullWidth
                                                >
                                                    {dataDPRK.map(option => (
                                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <RadioGroup
                                                row={!loading}
                                                aria-labelledby="demo-radio-buttons-group-label"
                                                defaultValue={statusDPRK}
                                                value={statusDPRK}
                                                name="radio-buttons-group"
                                            >
                                                {loading ? (
                                                    <Skeleton variant="text" />
                                                ) : (
                                                    status.length == 0 ? (
                                                        <div className='selected-empty'>Belum Ada Data</div>
                                                    ) : (
                                                        status.map(row => (
                                                            <FormControlLabel
                                                                name="gender"
                                                                onChange={e => setStatusDPRK(e.target.value)}
                                                                value={row.id.toString()}
                                                                control={<Radio />}
                                                                label={row.name}
                                                                key={row.id} />
                                                        ))
                                                    )
                                                )}
                                            </RadioGroup>
                                        </div>
                                    </div>
                                </AccordionDetails>
                            </Accordion>}
                            <div className="inner-button text-left">
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={handleSubmit}
                                    disabled={loading}
                                    fullWidth
                                >
                                    Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                                </Button>
                            </div>
                        </div> : null}
                </TabPanel>
            </SwipeableViews>

            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">

                </div>
            </div>

            <ToastContainer />

            <FilterVoters filterOpen={filterOpen} handleClose={handleClose} loadingFilter={loadingFilter} handleSubmitFilter={handleSubmitFilter} />

        </div>
    )

}

