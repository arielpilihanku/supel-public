import React from "react";

export default function Kandidat() {

    const movies = [
        "Presiden",
        "Gubernur",
        "Bupati",
        "DPR",
        "DPD",
        "DPRA",
        "DPRK"
    ];

    return (
        <>
            <div className="container-kandidat">
                {movies.map(src => (
                    <div key={src} className="card">
                        <img className="icon-kandidat" src='/person.png' alt='kandidat' />
                        <span className="title">{src}</span>
                    </div>
                ))}
            </div>
        </>
    );
}