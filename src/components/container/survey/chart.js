import React from "react";
import Api from "../../../libraries/api";
import { Chart } from "react-google-charts";
import CircularProgress from '@material-ui/core/CircularProgress';
import Slider from '@material-ui/core/Slider';
import FilterListIcon from '@material-ui/icons/FilterList';
import { Button } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';
import ClearIcon from '@material-ui/icons/Clear';
import { useDispatch, useSelector } from "react-redux";
import { payloadProvice } from "../../../redux/actions/area";
import { payloadKabupaten } from "../../../redux/actions/kabupaten";
import { payloadKecamatan } from "../../../redux/actions/kecamatan";
import { payloadKelurahan } from "../../../redux/actions/kelurahan";

function ChartKandidat() {

    const { loadingProvince, errorProvince, dataProvince } = useSelector((state) => state.province_state);
    const { loadingKabupaten, errorKabupaten, dataKabupaten } = useSelector((state) => state.kabupaten_state);
    const { loadingKecamatan, errorKecamatan, dataKecamatan } = useSelector((state) => state.kecamatan_state);
    const { loadingKelurahan, errorKelurahan, dataKelurahan } = useSelector((state) => state.kelurahan_state);

    const [loading, setLoading] = React.useState(false);
    const [chart, setChart] = React.useState([]);
    const [colors, setColors] = React.useState([]);
    const [lists, setLists] = React.useState([]);
    const [filterOpen, setFilterOpen] = React.useState(false);
    const [kodeProvince, setKodeProvince] = React.useState('11');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodeKecamatan, setKodeKecamatan] = React.useState('');
    const [kodeDesa, setKodeDesa] = React.useState('');
    const [loadingFilter, setLoadingFilter] = React.useState(false);
    const [areaFilter, setAreaFilter] = React.useState([]);

    const dispatch = useDispatch();


    React.useEffect(() => {

        setTimeout(() => {
           
            pushAreaFilter();
        }, 3000);

        fetchChart();
        dispatch(payloadProvice());
        dispatch(payloadKabupaten());
        dispatch(payloadKecamatan(''));
        dispatch(payloadKelurahan(''));


    }, [dispatch]);

    const fetchChart = async () => {

        setLoading(true);

        Api.get(`/dashboards/pie-chart`).then(resp => {


            if (resp) {

                const data = resp;

                setChart(data.data);
                setColors(data.colors);
                setLists(data.list);

                setLoading(false);

            }

        }).catch(err => {
            setLoading(false);
            console.log(err);
        });
    };


    const handleOpen = () => {
        setFilterOpen(true);
    };

    const handleClose = () => {
        setFilterOpen(false);
    };

    function handleChange(e, prop) {

        const value = e.target.value;

        if (prop === 'kode_province') {
            setKodeProvince(value);
        }

        if (prop === 'kode_kabupaten') {
            setKodeKabupaten(value);
            dispatch(payloadKecamatan(value));
        }

        if (prop === 'kode_kecamatan') {
            setKodeKecamatan(value);
            dispatch(payloadKelurahan(value));
        }

    };

    const handleSubmitFilter = () => {
        setLoadingFilter(true);

        const kode = [kodeProvince];

        if (kodeKabupaten != '') {
            kode.push(kodeKabupaten);
        }

        if (kodeKecamatan != '') {
            kode.push(kodeKecamatan);
        }

        if (kodeDesa != '') {
            kode.push(kodeDesa);
        }

        let formData = new FormData();
        kode.forEach(kd => formData.append('kode[]', kd))

        Api.putFile('/find/areas', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setAreaFilter(Object.keys(resp).map(key => resp[key]));

        }).catch(err => {
            console.log(err);
        });
        setLoadingFilter(false);
        setFilterOpen(false);

        setLoading(true);

        Api.get(`/dashboards/pie-chart?kode_kabupaten=${kodeKabupaten}&kode_kecamatan=${kodeKecamatan}&kode_desa=${kodeDesa}`).then(resp => {


            if (resp) {

                const data = resp;

                setChart(data.data);
                setColors(data.colors);
                setLists(data.list);

                setLoading(false);

            }

        }).catch(err => {
            setLoading(false);
            console.log(err);
        });

    };

    const pushAreaFilter = (province) => {

        const kode = [province];

        let formData = new FormData();
        kode.forEach(kd => formData.append('kode[]', kd))

        Api.putFile('/find/areas', {
            method: 'POST',
            body: formData
        }).then(resp => {

            setAreaFilter(Object.keys(resp).map(key => resp[key]));

        }).catch(err => {
            console.log(err);
        });

    };

    const handleClearFilter = () => {
        fetchChart();
        setKodeProvince('11');
        setKodeKabupaten('');
        setKodeKecamatan('');
        setKodeDesa('');
        pushAreaFilter();
    };

    const options = {
        pieHole: 0.4,
        // is3D: false,
        colors: colors,
        legend: 'none',
        chartArea: {
            left: 5,
            top: 20,
            width: '100%',
            height: '350',
        }
    };

    function valuetext(value) {
        return `${value}%`;
    }

    return (
        <>
            <Chart
                chartType="PieChart"
                width="100%"
                height="400px"
                data={chart}
                options={options}
            />

            <div className="inner-filter">
                <Button className="filter-btn" onClick={handleOpen}>
                    <FilterListIcon />
                    Filter
                </Button>
                {areaFilter.map(option => (
                    <Button className="filter-title" key={option.id}>
                        {option.nama}
                    </Button>
                ))}
                {areaFilter.length > 1 && <Button className="clear-btn" onClick={handleClearFilter}>
                    <ClearIcon />
                    Clear
                </Button>}
            </div>


            {loading ? (
                <CircularProgress />
            ) : (
                lists.map(option => (
                    <div className="list-kandidat" key={option.name}>
                        <div className="list1">
                            <span className="partai">{option.partai}</span>
                            <span className="campaign">{option.campaign}</span>
                        </div>
                        <div className="list2">
                            {option.name}
                        </div>
                        <div className="list3">
                            <Slider
                                defaultValue={option.percentage}
                                valueLabelFormat={valuetext}
                                aria-labelledby="discrete-slider-always"
                                step={10}
                                disabled
                                valueLabelDisplay="on"
                            />
                        </div>
                        <div className="pemilih">
                            <span>{parseInt(option.total).toLocaleString()}</span>
                            <span>Pendukung</span>
                        </div>
                    </div>
                ))
            )
            }
            <Dialog
                maxWidth='sm'
                fullWidth={true}
                open={filterOpen}
                onClose={handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Filter Area</DialogTitle>
                <DialogContent>
                    <div className="form-group">
                        <label>Provinsi <span className="required">*</span></label>
                        <TextField variant='outlined'
                            select
                            id='kode_province'
                            name='kode_province'
                            label='Provinsi'
                            onChange={e => handleChange(e, 'kode_province')}
                            value={kodeProvince}
                            defaultValue={''}
                            fullWidth
                        >
                            {loadingProvince ?
                                (<CircularProgress />) : dataProvince ? (dataProvince.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                        {option.province}
                                    </MenuItem>
                                ))) : errorProvince ? (<p>{errorProvince}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                    <div className="form-group">
                        <label>Kabupaten</label>
                        <TextField variant='outlined'
                            select
                            id='kode_kabupaten'
                            name='kode_kabupaten'
                            label='Kabupaten'
                            onChange={e => handleChange(e, 'kode_kabupaten')}
                            value={kodeKabupaten}
                            fullWidth
                        >
                             {loadingKabupaten ?
                                (<CircularProgress />) : dataKabupaten ? (dataKabupaten.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                        {option.kabupaten}
                                    </MenuItem>
                                ))) : errorKabupaten ? (<p>{errorKabupaten}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                    <div className="form-group">
                        <label>Kecamatan</label>
                        <TextField variant='outlined'
                            select
                            id='kode_kecamatan'
                            name='kode_kecamatan'
                            label='Kecamatan'
                            onChange={e => handleChange(e, 'kode_kecamatan')}
                            value={kodeKecamatan}
                            fullWidth
                        >
                            {loadingKecamatan ?
                                (<CircularProgress />) : dataKecamatan ? (dataKecamatan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kecamatan}>
                                        {option.kecamatan}
                                    </MenuItem>
                                ))) : errorKecamatan ? (<p>{errorKecamatan}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                    <div className="form-group">
                        <label>Desa</label>
                        <TextField variant='outlined'
                            select
                            id='kode_desa'
                            name='kode_desa'
                            label='Desa'
                            onChange={e => setKodeDesa(e.target.value)}
                            value={kodeDesa}
                            fullWidth
                        >
                           {loadingKelurahan ?
                                (<CircularProgress />) : dataKelurahan ? (dataKelurahan.map(option => (
                                    <MenuItem key={parseInt(option.id)} value={option.kode_kelurahan}>
                                        {option.kelurahan}
                                    </MenuItem>
                                ))) : errorKelurahan ? (<p>{errorKelurahan}</p>) : (<p>Data Not Found!</p>)}
                        </TextField>
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="default" className="btn btn-info" onClick={handleClose}>
                        Keluar
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        className="btn btn-info"
                        onClick={handleSubmitFilter}
                        disabled={loadingFilter}
                    >
                        Terapkan{loadingFilter && <i className="fa fa-spinner fa-spin"> </i>}
                    </Button>
                </DialogActions>
            </Dialog>
        </>

    );
}

export default ChartKandidat;