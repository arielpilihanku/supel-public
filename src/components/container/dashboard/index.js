import React, {Component} from 'react';
import ContentDashboard from "../content-dashboard";
import Charts from "../content-dashboard/charts";
import NotFound from "../not-found";
import {Switch} from 'react-router-dom'
import Sidebar from "../sidebar";
import Navbar from "../navbar";
import Users from "../users";
import UsersAdd from "../users/add";
import UsersEdit from "../users/edit";
import Roles from "../roles";
import RolesEdit from "../roles/edit";
import Articles from '../article/index';
import ArticleAdd from '../article/add';
import ArticleEdit from '../article/edit';
import Campaign from '../campaign/index';
import CampaignAdd from '../campaign/add';
import CampaignEdit from '../campaign/edit';
import Timses from '../timses/index';
import Capel from '../capel/index';
import CapelBlack from '../capelblack/index';

import AuthHelper from "../../../libraries/auth-helper";

import {PrivateRoute} from '../../../libraries/route-handling'
import {bindActionCreators} from "redux";
import {RootActions} from "../../../shared/root-action";
import {connect} from "react-redux";
import {history} from "../../../shared/configure-store";


import Home from "../home";
import Partai from '../partai';
import PartaiAdd from '../partai/add';
import PartaiEdit from '../partai/edit';
import Dapil from '../dapil';
import DapilAdd from '../dapil/add';
import DapilEdit from '../dapil/edit';
import DapilWilayahAdd from '../dapil/add-wilayah';
import Wilayah from '../wilayah';
import Banner from '../banner';
import BannerAdd from '../banner/add';
import BannerEdit from '../banner/edit';
import Capres from '../capres';
import CapresAdd from '../capres/add';
import CapresEdit from '../capres/edit';
import Simpatisan from '../simpatisan';
import SimpatisanAdd from '../simpatisan/add';
import SimpatisanEdit from '../simpatisan/edit';


const styleSide = {
    maxWidth: '280px',
    background: '#fbfbfb'
};

const styleDashboard = {
    zIndex: '5',
    position: 'relative',
};

class Dashboard extends Component {

    /*constructor(props) {

        super(props);

    }*/

    componentDidMount() {
        if(AuthHelper.isLoggedIn()){
            this.props.setProfile(AuthHelper.getProfile());
        }else{
            history.push('/login');
        }
    }

    render() {
        return (

            <div>
                <Navbar />

                <main className="dashboard">

                    <div className="container-fluid">

                        <div className="row">

                            <div className="col-xl px-0" style={styleSide}>

                                <Sidebar/>

                            </div>

                            <div className="col-xl dashbboard-container" style={styleDashboard}>

                                <Switch>

                                    <PrivateRoute exact path="/" component={Home}/>
                                    <PrivateRoute exact path="/dashboard" component={ContentDashboard}/>

                                    <PrivateRoute exact path="/articles" component={Articles}/>
                                    <PrivateRoute path="/articles/add" component={ArticleAdd}/>
                                    <PrivateRoute path="/articles/edit/:id" component={ArticleEdit}/>

                                    <PrivateRoute exact path="/partai" component={Partai}/>
                                    <PrivateRoute path="/partai/add" component={PartaiAdd}/>
                                    <PrivateRoute path="/partai/edit/:id" component={PartaiEdit}/>

                                    <PrivateRoute exact path="/banner" component={Banner}/>
                                    <PrivateRoute path="/banner/add" component={BannerAdd}/>
                                    <PrivateRoute path="/banner/edit/:id" component={BannerEdit}/>

                                    <PrivateRoute exact path="/capres" component={Capres}/>
                                    <PrivateRoute path="/capres/add" component={CapresAdd}/>
                                    <PrivateRoute path="/capres/edit/:id" component={CapresEdit}/>

                                    <PrivateRoute exact path="/dapil" component={Dapil}/>
                                    <PrivateRoute path="/dapil/add" component={DapilAdd}/>
                                    <PrivateRoute path="/dapil/edit/:id" component={DapilEdit}/>
                                    <PrivateRoute path="/dapil-wilayah/add/:id" component={DapilWilayahAdd}/>

                                    <PrivateRoute exact path="/wilayah" component={Wilayah}/>

                                    <PrivateRoute exact path="/campaign" component={Campaign}/>
                                    <PrivateRoute path="/campaign/add" component={CampaignAdd}/>
                                    <PrivateRoute path="/campaign/edit/:id" component={CampaignEdit}/>

                                    <PrivateRoute exact path="/timses" component={Timses}/>
                                    <PrivateRoute exact path="/timses-blacklist" component={CapelBlack}/>
                                    <PrivateRoute exact path="/capel" component={Capel}/>
                                    <PrivateRoute exact path="/peta" component={ContentDashboard}/>
                                    <PrivateRoute exact path="/calculation" component={Charts}/>

                                    <PrivateRoute exact path="/simpatisan" component={Simpatisan}/>
                                    <PrivateRoute path="/simpatisan/add" component={SimpatisanAdd}/>
                                    <PrivateRoute path="/simpatisan/edit/:id" component={SimpatisanEdit}/>

                                    <PrivateRoute exact path="/users" component={Users}/>
                                    <PrivateRoute path="/users/add" component={UsersAdd}/>
                                    <PrivateRoute path="/users/edit/:id" component={UsersEdit}/>

                                    <PrivateRoute exact path="/roles" component={Roles}/>
                                    <PrivateRoute path="/roles/permission/:id" component={RolesEdit}/>
                            

                                    <PrivateRoute component={NotFound}/>
                                </Switch>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

        )

    }
}

const mapStateToProps = (state) => {

    return {

        profile_state: state.profile_state,

        role_state: state.role_state,

        permission_state: state.permission_state,

    };

};

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators(RootActions, dispatch)

};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

