import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Chart from 'chart.js';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from "@material-ui/core/TextField";
import { DateRangePicker } from "@shinabr2/react-material-daterange-picker";
import moment from "moment/moment";
import Api from "../../../libraries/api";

class Charts extends Component {
    constructor(props) {

        super(props);
        this.state = {
            loading: false,
            openDate: false,
            startDate: null,
            endDate: null,
            type: '',
            dateRange: {},
            data: {
                labels: [],
                approval: {
                    data: [],
                },
                report: {
                    data: [],
                },
            },
        }
    }

    componentDidMount() {
        document.title = 'Abadi - Chart';

        this.setState({
            loading: true,
        });

        this.__fetch();

    }

    buildChart(){
        let data = {
            labels: this.state.data.labels,
            datasets: [{
                label: 'Report',
                data: this.state.data.report.data,
                backgroundColor: '#1ebead',
                borderColor: '#1ebead',
                borderWidth: 1
            },
                {
                    label: 'Approval',
                    data: this.state.data.approval.data,
                    backgroundColor: '#5a7dfd',
                    borderColor: '#5a7dfd',
                    borderWidth: 1
                }]
        };

        let options = {
            scales: {
                xAxes: [{
                    gridLines: {
                        offsetGridLines: false
                    }
                }],
                yAxes: [{
                    gridLines: {
                        offsetGridLines: false
                    }
                }]
            }
        };

        let ctxHBar = document.getElementById("horizontal_bar_chart");
        let myHorizBarChart = new Chart(ctxHBar, {
            type: 'bar',
            data: data,
            options: options
        });

        this.setState({
            data: data, options: options
        });

    };

    __fetch = () => {
        let params = '/dashboards/chart-issued';

        if(this.state.type) {
            params = '/dashboards/chart-issued?type='+this.state.type+'&startDate='+moment(this.state.startDate).format('YYY-MM-DD')+'&endDate='+moment(this.state.endDate).format('YYY-MM-DD');
        }

        Api.get(params).then(resp => {
            if (resp.data) {

                let data = resp.data;

                this.setState({
                    data: {
                        labels: data.lables,
                        approval: {
                            data: data.approval.data,
                        },
                        report: {
                            data: data.report.data,
                        },
                    },
                });

                this.buildChart();
            }
        }).catch(err => {
            console.log(err);
        });

    };

    handleChange (e, prop){

        this.setState({

            [prop]: e.target.value,

        });

    };

    setDateRange = (date, prop) => {

        this.setState({
                startDate: date.startDate,
                endDate: date.endDate,
                openDate: false,
            }
        );
    };

    openDate = (date, prop) => {

        this.setState({
                [prop]: true,
            }, () => {
            this.__fetch();
        })
    };


    render() {

        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h2 className="page-title">Perhitungan Suara</h2>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className='breadcrumb-item'><Link to='/' >Home</Link></li>
                            <li className='breadcrumb-item active' aria-current='page'>Perhitungan Suara</li>
                        </ol>
                    </nav>
                    <div className='table-wrapper'>
                        <div className='row'>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <TextField variant="outlined"
                                               select
                                               id="type"
                                               name="type"
                                               label="Per"
                                               onChange={(e) => this.handleChange(e, 'type')}
                                               value={this.state.type}
                                               fullWidth
                                    >
                                        <MenuItem value="instruction_date">
                                            Instruction Date
                                        </MenuItem>
                                        <MenuItem value="issued_date">
                                            Issued Date
                                        </MenuItem>
                                    </TextField>
                                </div>
                            </div>
                            <div className='col-md-6'>
                                <button className='btn btn-date'
                                        onClick={(e) => this.openDate(e,'openDate')}
                                        disabled={this.state.type === ''}
                                >
                                    {this.state.startDate ? moment(this.state.startDate).format('DD MMM YYYY') : 'mulai'} - {this.state.endDate ? moment(this.state.endDate).format('DD MMM YYYY') : 'akhir'}
                                    <i className="fas fa-calendar-week"> </i>
                                </button>

                            </div>
                            <div className="col-12">
                                <DateRangePicker
                                    open={this.state.openDate}
                                    onChange={(range) => this.setDateRange(range, 'dateTransaction')}
                                />
                            </div>

                            <div className='col-12'>
                                <canvas
                                    id="horizontal_bar_chart"
                                > </canvas>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )

    }
}

export default Charts;