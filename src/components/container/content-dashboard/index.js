import React,{Component} from 'react';
import img from '../../../images/home.png';
import {Link} from 'react-router-dom';
import Api from "../../../libraries/api";

class ContentDashboard extends Component {
    constructor(props) {

        super(props);
        this.state = {
            loading: false,
            loading2: false,
            loading3: false,
            loading4: false,
            loading5: false,
            loading6: false,
            loading7: false,
            loading8: false,
            loading9: false,
            loading10: false,
            loading11: false,
            loading12: false,

            data1: 0,
            data2: '',
            data3: '',
            data4: '',
            data5: '',
            data6: 0,
            data7: 0,
            data8: 0,
            data9: 0,
            data10: 0,
            data11: 0,
            data12a: 0,
            data12b: 0,
        }
    }

    componentDidMount() {
        document.title = 'Abadi - Dashboard';

        this.setState({
            loading1: true,
            loading2: true,
            loading3: true,
            loading4: true,
            loading5: true,
            loading12: true,
        });

        Api.get('/dashboards/pending-issued-invoice').then(resp => {

            if (resp.data.length > 0) {

                this.setState({
                    data1: resp.data[0].total,
                    loading1: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading1: false
            })
        });

        Api.get('/dashboards/pending-payment-invoice').then(resp => {

            if (resp.data) {

                this.setState({
                    data2: resp.data,
                    loading2: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading2: false
            })
        });

        Api.get('/dashboards/invoice-issued-week').then(resp => {

            if (resp.data) {

                this.setState({
                    data3: resp.data,
                    loading3: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading3: false
            })
        });

        Api.get('/dashboards/invoice-issued-month').then(resp => {

            if (resp.data) {

                this.setState({
                    data4: resp.data,
                    loading4: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading4: false
            })
        });

        Api.get('/dashboards/complete-payment').then(resp => {

            if (resp.data) {

                this.setState({
                    data5: resp.data,
                    loading5: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading5: false
            })
        });

        Api.get('/dashboards/case-this-week').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data6: resp.data[0].total,
                    loading6: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading6: false
            })
        });

        Api.get('/dashboards/case-this-month').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data7: resp.data[0].total,
                    loading7: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading7: false
            })
        });

        Api.get('/dashboards/issue-report-week').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data8: resp.data[0].total,
                    loading8: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading8: false
            })
        });

        Api.get('/dashboards/issue-report-month').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data9: resp.data[0].total,
                    loading9: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading9: false
            })
        });

        Api.get('/dashboards/count-report').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data10: resp.data[0].total,
                    loading10: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading10: false
            })
        });

        Api.get('/dashboards/count-approval').then(resp => {

            if (resp.data.length > 0) {
                this.setState({
                    data11: resp.data[0].total,
                    loading11: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading11: false
            })
        });

        Api.get('/dashboards/follow-up').then(resp => {

            if (resp.data) {
                this.setState({
                    data12a: resp.data.report,
                    data12b: resp.data.approval,
                    loading12: false
                })

            }

        }).catch(err => {
            console.log(err);
            this.setState({
                loading12: false
            })
        });
    }

    render() {

        return (

            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h2 className="page-title">Peta Suara</h2>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Peta Suara</li>                        </ol>
                    </nav>

                    <div className="card-home mb-3">
                        <div className="row justify-content-between align-items-center">
                            <div className="col-md-7 ">
                                <blockquote>Welcome Back!</blockquote>
                                <p></p>
                            </div>
                            <div className="col-md-5 col-lg-4">
                                <img src={img} alt='welcome' className="img-fluid" />
                            </div>
                        </div>
                    </div>

                

           
                </div>

            </div>

        )

    }
}

export default ContentDashboard;