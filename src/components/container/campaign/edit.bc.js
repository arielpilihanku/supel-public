import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

class CampaignEdit extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},
            campaign: false,
            survey: false,
            count: false,

            id: '',
            group: '',
            subgroup: '',
            kode_subgroup: '',
            kode_province: '',
            kode_kabupaten: '',
            kode_partai: '',
            kode_dapil: '',
            name: '',

            rows: [],
            groups: [],
            subgroups: [],
            provinces: [],
            kabupaten: [],
            partai: [],
            dapil: [],
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Halaman Edit Kandidat';

        const id = this.props.match.params.id;

        Api.get('/campaigns/' + id).then(resp => {

            if (resp.data) {
                let data = resp.data;
                this.setState({
                    id: data.id,
                    name: data.name,
                    group: data.group_campaign_id,
                    subgroup: data.subgroup_campaign_id,
                    kode_subgroup: data.kode_subgroup_campaign,
                    kode_province: data.kode_provinsi,
                    kode_kabupaten: data.kode_kabupaten,
                    kode_partai: data.kode_partai,
                    kode_dapil: data.kode_dapil,
                    campaign: data.campaign,
                    survey: data.survey,
                    count: data.count,
                });

                this.__fetchDapil(data.kode_subgroup_campaign);
                this.__fetchDataKabupaten(data.kode_provinsi);
                this.__fetchSubGroup(data.group_campaign_id);
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/group/campaigns').then(resp => {

            if (resp.data) {
                let groups = resp.data;

                this.setState({
                    loading: false,
                    groups: groups,
                })
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/subgroup/campaigns').then(resp => {

            if (resp.data) {
                let groups = resp.data;

                this.setState({
                    loading: false,
                    subgroups: groups,
                })
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/province/areas').then(resp => {


            if (resp) {

                this.setState({
                    provinces: Object.keys(resp).map(key => resp[key]),
                })

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/partai').then(resp => {

            const data = resp.data;

            if (data) {

                this.setState({
                    partai: Object.keys(data).map(key => data[key]),
                })

            }

        }).catch(err => {
            console.log(err);
        });

    }

    __fetchSubGroup = groupid => {

        Api.get('/subgroup/campaigns?group=' + groupid).then(resp => {


            if (resp.data) {

                this.setState({
                    subgroups: resp.data,
                })

            }

        }).catch(err => {
            console.log(err);
        });
    };

    __fetchDataKabupaten = province => {

        Api.get('/kabupaten/areas?province=' + province).then(resp => {


            if (resp) {

                this.setState({
                    kabupaten: Object.keys(resp).map(key => resp[key])
                })

            }

        }).catch(err => {
            console.log(err);
        });
    };

    __fetchDapil = subgroup => {

        Api.get('/dapil/campaigns?subgroup=' + subgroup).then(resp => {

            const data = resp.data;

            if (data) {

                this.setState({
                    dapil: Object.keys(data).map(key => data[key])
                })

            }

        }).catch(err => {
            console.log(err);
        });
    };

    __fetchFindSubGroup = id => {

        Api.get('/subgroup/campaigns/' + id).then(resp => {

            const data = resp.data;

            if (data) {

                this.setState({
                    kode_subgroup: data.kode
                });

                this.__fetchDapil(data.kode);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        });

        if (prop === 'group') {
            this.__fetchSubGroup(e.target.value);
        }

        if (prop === 'subgroup') {
            this.__fetchFindSubGroup(e.target.value);
        }

    };

    handleChangeProvince(e, prop) {

        this.setState({

            kode_province: e.target.value,

        });

        this.__fetchDataKabupaten(e.target.value);

    };

    handleChangeKabupaten(e, prop) {

        this.setState({

            kode_kabupaten: e.target.value,

        });

    };


    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleSubmit = (e) => {

        e.preventDefault();

        if (!validator.allValid()) {

            this.setState({
                errors: validator.getErrorMessages()
            }
            );

            return false;

        }

        this.setState({
            errors: {},
            loadingButton: true,
        }
        );

        const params = {
            group_campaign_id: this.state.group,
            subgroup_campaign_id: this.state.subgroup,
            kode_provinsi: this.state.kode_province,
            kode_kabupaten: this.state.kode_kabupaten,
            kode_dapil: this.state.kode_dapil,
            kode_partai: this.state.kode_partai,
            name: this.state.name,
            campaign: this.state.campaign ? 1 : 0,
            survey: this.state.survey ? 1 : 0,
            count: this.state.count ? 1 : 0,
        };

        Api.patch('/campaigns/' + this.state.id, params).then(resp => {

            this.setState({
                loadingButton: false,
            });

            history.push('/campaign');

            this.showMessage(true, 'Berhasil merubah kandidat.');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                this.setState({
                    errors: err.errors,
                    loadingButton: false
                });

                this.showMessage(false, 'Invalid format data');
            }
        });

    };


    handleGo = (link) => {
        history.push(link);
    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Rubah Kandidat</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item"><Link to="/regulations" >Kandidat</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Rubah</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name="add" id="addUser" className="row" noValidate>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Group Pemilihan<span className="required">*</span></label>
                                    <RadioGroup
                                        row={!this.state.loading}
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        value={this.state.group.toString()}
                                        defaultValue={this.state.group.toString()}
                                        name="radio-group"
                                    >
                                        {this.state.loading ? (
                                            <Skeleton variant="text" />
                                        ) : (
                                            this.state.groups.length == 0 ? (
                                                <div className='selected-empty'>Belum Ada Data</div>
                                            ) : (

                                                this.state.groups.map(row => (

                                                    <FormControlLabel
                                                        name="group"
                                                        onChange={(e) => this.handleChange(e, 'group')}
                                                        value={row.id.toString()}
                                                        control={<Radio />}
                                                        label={row.name}
                                                        key={row.id} />


                                                ))
                                            )
                                        )}

                                    </RadioGroup>
                                    {validator.message('group', this.state.group, 'required')}
                                    <div className='text-danger'>{this.state.errors.group}</div>
                                </div>
                            </div>
                            <div className="col-md-6"></div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Pemilihan<span className="required">*</span></label>
                                    <RadioGroup
                                        row={!this.state.loading}
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        value={this.state.subgroup.toString()}
                                        defaultValue={this.state.subgroup.toString()}
                                        name="radio-subgroup"
                                    >
                                        {this.state.loading ? (
                                            <Skeleton variant="text" />
                                        ) : (
                                            this.state.subgroups.length == 0 ? (
                                                <div className='selected-empty'>Belum Ada Data</div>
                                            ) : (

                                                this.state.subgroups.map(row => (

                                                    <FormControlLabel
                                                        name="subgroup"
                                                        onChange={(e) => this.handleChange(e, 'subgroup')}
                                                        value={row.id.toString()}
                                                        control={<Radio />}
                                                        label={row.name}
                                                        key={row.id} />


                                                ))
                                            )
                                        )}

                                    </RadioGroup>
                                    {validator.message('subgroup', this.state.subgroup, 'required')}
                                    <div className='text-danger'>{this.state.errors.subgroup}</div>
                                </div>
                            </div>
                            <div className="col-md-6"></div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Produk Di Beli <span className="required"></span></label>
                                    <FormGroup>
                                        <FormControlLabel
                                            control={<Checkbox checked={this.state.campaign} onChange={(e) => this.setState({campaign: true})} name="campaign" />}
                                            label="Kampanye"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox checked={this.state.survey} onChange={(e) => this.setState({survey: true})} name="survey" />}
                                            label="Survey Pemilih"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox checked={this.state.count} onChange={(e) => this.setState({count: true})} name="count" />}
                                            label="Quick Count"
                                        />
                                    </FormGroup>

                                    <div className='text-danger'></div>
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            {(this.state.group == 2 && this.state.subgroup != 4) &&
                                <>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label>Partai</label>
                                            <TextField variant='outlined'
                                                select
                                                id='kode_partai'
                                                name='kode_partai'
                                                label='Partai'
                                                onChange={(e) => this.handleChange(e, 'kode_partai')}
                                                value={this.state.kode_partai}
                                                fullWidth
                                            >
                                                {this.state.partai.map(option => (
                                                    <MenuItem key={parseInt(option.id)} value={option.real_id}>
                                                        {option.name}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </div>

                                    </div>
                                    <div className="col-md-6">

                                    </div>
                                </>
                            }

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Provinsi <span className="required">*</span></label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_province'
                                        name='kode_province'
                                        label='Provinsi'
                                        onChange={(e) => this.handleChangeProvince(e, 'kode_province')}
                                        value={this.state.kode_province}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {this.state.provinces.map(option => (
                                            <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                                {option.province}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    {validator.message('kode_province', this.state.kode_province, 'required')}
                                    <div className='text-danger'>{this.state.errors.kode_province}</div>
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            {(this.state.subgroup == 2 || this.state.subgroup == 6) &&
                                <>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label>Kabupaten</label>
                                            <TextField variant='outlined'
                                                select
                                                id='kode_kabupaten'
                                                name='kode_kabupaten'
                                                label='Kabupaten'
                                                onChange={(e) => this.handleChangeKabupaten(e, 'kode_kabupaten')}
                                                value={this.state.kode_kabupaten}
                                                fullWidth
                                            >
                                                {this.state.kabupaten.map(option => (
                                                    <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                                        {option.kabupaten}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </div>

                                    </div>
                                    <div className="col-md-6">

                                    </div>
                                </>
                            }

                            {(this.state.group == 2 && this.state.subgroup != 4) &&
                                <>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label>Dapil</label>
                                            <TextField variant='outlined'
                                                select
                                                id='kode_dapil'
                                                name='kode_dapil'
                                                label='Dapil'
                                                onChange={(e) => this.handleChange(e, 'kode_dapil')}
                                                value={this.state.kode_dapil}
                                                fullWidth
                                            >
                                                {this.state.dapil.map(option => (
                                                    <MenuItem key={parseInt(option.id)} value={option.real_id}>
                                                        {option.name}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        {this.state.dapil.length == 0 &&
                                            <Button
                                                variant="contained"
                                                className="add-kabupaten mr-3"
                                                onClick={() => this.handleGo('/dapil')}
                                            >
                                                Buat Dapil
                                            </Button>
                                        }
                                    </div>
                                </>
                            }

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Nama Kandidat <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='name'
                                        name="name"
                                        label="Input Nama Kandidat"
                                        onChange={(e) => this.handleChange(e, 'name')}
                                        value={this.state.name}
                                        fullWidth
                                    />
                                    {validator.message('name', this.state.name, 'required')}
                                    <div className='text-danger'>{this.state.errors.name}</div>
                                </div>
                            </div>
                            <div className="col-md-6"></div>

                            <div className="col-12 text-left">
                                <Button
                                    variant="contained"
                                    className="mr-3"
                                    onClick={() => this.handleGo('/campaign')}
                                >
                                    Kembali
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={this.handleSubmit}
                                    disabled={this.state.loadingButton && 'disabled'}
                                >
                                    Simpan{this.state.loadingButton && <i className="fa fa-spinner fa-spin"> </i>}
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default CampaignEdit;
