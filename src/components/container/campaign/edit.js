import React, { Component } from "react";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import TextField from "@material-ui/core/TextField";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';
import MenuItem from '@material-ui/core/MenuItem';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';


require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function CampaignEdit(props) {

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [id, setId] = React.useState('');
    const [campaign, setCampaign] = React.useState(false);
    const [survey, setSurvey] = React.useState(false);
    const [count, setCount] = React.useState(false);
    const [group, setGroup] = React.useState('');
    const [subgroup, setSubgroup] = React.useState('');
    const [kodeSubGroup, setKodeSubGroup] = React.useState('');
    const [kodeProvince, setKodeProvince] = React.useState('');
    const [kodeKabupaten, setKodeKabupaten] = React.useState('');
    const [kodePartai, setKodePartai] = React.useState('');
    const [kodeDapil, setKodeDapil] = React.useState('');
    const [name, setName] = React.useState('');
    const [date, setDate] = React.useState(null);
    const [warna, setWarna] = React.useState('');
    const [singkatan, setSingkatan] = React.useState('');
    const [slogan, setSlogan] = React.useState('');
    const [groups, setGroups] = React.useState([]);
    const [subgroups, setSubgroups] = React.useState([]);
    const [provinces, setProvinces] = React.useState([]);
    const [kabupaten, setKabupaten] = React.useState([]);
    const [partai, setPartai] = React.useState([]);
    const [dapil, setDapil] = React.useState([]);
    const [loadingGroup, setLoadingGroup] = React.useState(false);
    const [loadingSubGroup, setLoadingSubGroup] = React.useState(false);
    const [image, setImage] = React.useState({
        file: [],
        filepreview: null,
    });

    const [invalidImage, setinvalidImage] = React.useState(null);
    let reader = new FileReader();

    const [photo, setPhoto] = React.useState('');
    const [colors, setColors] = React.useState([]);
    const [loadingColors, setLoadingColors] = React.useState(false);



    React.useEffect(() => {

        document.title = 'Admin suarapemilu - Halaman Edit Kandidat';

        const idKandidat = props.match.params.id;

        Api.get('/campaigns/' + idKandidat).then(resp => {

            if (resp.data) {
                let dataCampaign = resp.data;
                setId(dataCampaign.id);
                setName(dataCampaign.name);
                setSingkatan(dataCampaign.singkatan);
                setSlogan(dataCampaign.slogan);
                setGroup(dataCampaign.group_campaign_id);
                setSubgroup(dataCampaign.subgroup_campaign_id);
                setKodeSubGroup(dataCampaign.kode_subgroup_campaign);
                setKodeProvince(dataCampaign.kode_provinsi);
                if (dataCampaign.kode_kabupaten != null) {
                    setKodeKabupaten(dataCampaign.kode_kabupaten);
                }
                if (dataCampaign.kode_partai != null) {
                    setKodePartai(dataCampaign.kode_partai);
                }
                if (dataCampaign.kode_dapil != null) {
                    setKodeDapil(dataCampaign.kode_dapil);
                }
                setCampaign(dataCampaign.campaign);
                setSurvey(dataCampaign.survey);
                setCount(dataCampaign.count);
                setPhoto(dataCampaign.image);

                fetchDapil(dataCampaign.kode_subgroup_campaign);
                fetchColors(dataCampaign.subgroup_campaign_id);
                fetchDataKabupaten(dataCampaign.kode_provinsi);
                fetchSubGroup(dataCampaign.group_campaign_id);

                setWarna(dataCampaign.warna);
                setDate(dataCampaign.date_campaign);
            }

        }).catch(err => {
            console.log(err);
        });

        setLoadingGroup(true);
        setLoadingSubGroup(true);

        Api.get('/group/campaigns').then(resp => {

            if (resp.data) {

                setLoadingGroup(false);
                setGroups(resp.data);

            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/subgroup/campaigns').then(resp => {

            if (resp.data) {
                setLoadingSubGroup(false);
                setSubgroups(resp.data);
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/province/areas').then(resp => {


            if (resp) {
                setProvinces(Object.keys(resp).map(key => resp[key]));
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/partai?limit=0').then(resp => {

            const data = resp.data;

            if (data) {
                setPartai(Object.keys(data).map(key => data[key]));
            }

        }).catch(err => {
            console.log(err);
        });

    }, []);

    const fetchColors = async (subgroup) => {

        setLoadingColors(true);

        Api.get('/colors/campaigns?campaign=' + subgroup).then(resp => {


            if (resp) {

                setLoadingColors(false);
                setColors(Object.keys(resp).map(key => resp[key]));

            }

        }).catch(err => {
            console.log(err);
        });
    };


    const fetchSubGroup = async (groupid) => {

        Api.get('/subgroup/campaigns?group=' + groupid).then(resp => {


            if (resp.data) {

                setLoadingSubGroup(false);
                setSubgroups(resp.data);

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchDataKabupaten = async (province) => {

        Api.get('/kabupaten/areas?province=' + province).then(resp => {


            if (resp) {

                setKabupaten(Object.keys(resp).map(key => resp[key]));

            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchDapil = async (subgroup) => {

        Api.get('/dapil/campaigns?subgroup=' + subgroup).then(resp => {

            const data = resp.data;

            if (data) {
                setDapil(Object.keys(data).map(key => data[key]));
            }

        }).catch(err => {
            console.log(err);
        });
    };

    const fetchFindSubGroup = async (id) => {

        Api.get('/subgroup/campaigns/' + id).then(resp => {

            const data = resp.data;

            if (data) {
                setKodeSubGroup(data.kode);
                fetchDapil(data.kode);
            }

        }).catch(err => {
            console.log(err);
        });
    };

    function handleChangeWarna(e, prop) {

        setWarna(prop);

    };


    function handleChange(e, prop) {

        const value = e.target.value;

        if (prop === 'group') {
            setGroup(value);
            fetchSubGroup(value);
        }

        if (prop === 'subgroup') {
            setSubgroup(value);
            setKodeSubGroup(value);
            fetchFindSubGroup(value);
            fetchColors(value);
        }

        if (prop === 'kode_province') {
            setKodeProvince(value);
            fetchDataKabupaten(value);
        }

    };

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());

            return false;

        }

        setLoading(true);

        const cekSubGroup = [3, 5, 6];

        if (cekSubGroup.includes(parseInt(subgroup)) && kodeDapil == '') {
            setLoading(false);
            showMessage(false, 'Data dapil tidak boleh kosong!');
            return false;
        }

        if (cekSubGroup.includes(parseInt(subgroup)) && kodePartai == '') {
            setLoading(false);
            showMessage(false, 'Data partai tidak boleh kosong!');
            return false;
        }

        let formData = new FormData();
        formData.append('group_campaign_id', group);
        formData.append('subgroup_campaign_id', subgroup);
        formData.append('kode_provinsi', kodeProvince);
        formData.append('kode_kabupaten', kodeKabupaten);
        formData.append('kode_dapil', kodeDapil);
        formData.append('kode_partai', kodePartai);
        formData.append('name', name);
        formData.append('singkatan', singkatan);
        formData.append('slogan', slogan);
        formData.append('warna', warna);
        formData.append('campaign', campaign ? 1 : 0);
        formData.append('survey', survey ? 1 : 0);
        formData.append('count', count ? 1 : 0);
        formData.append('image', image.file);
        formData.append('date_campaign', moment(date).format('YYYY-MM-DD'))

        Api.putFile('/campaigns/' + id, {
            method: 'POST',
            body: formData
        }).then(resp => {

            setLoading(false);

            history.push('/campaign');

            showMessage(true, 'Kandidat berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setLoading(false);
                setErrors(err.errors);

                showMessage(false, 'Invalid format data');
            }
        });

    };


    const handleBack = () => {
        history.push('/campaign');
    };

    const handleGoDapil = () => {
        history.push('/dapil');
    };

    const onChangeImage = (event) => {
        const imageFile = event.target.files[0];
        const imageFileName = event.target.files[0].name;

        if (!imageFile) {
            setinvalidImage('Please select image.');
            return false;
        }

        if (!imageFile.name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG|gif)$/)) {
            setinvalidImage('Please select valid image JPG,JPEG,PNG');
            return false;
        }
        reader.onload = (e) => {
            const img = new Image();
            img.onload = () => {

                //------------- Resize img code ----------------------------------
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 437;
                var MAX_HEIGHT = 437;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                ctx.canvas.toBlob((blob) => {
                    const file = new File([blob], imageFileName, {
                        type: 'image/jpeg',
                        lastModified: Date.now()
                    });
                    setImage({
                        ...image,
                        file: file,
                        filepreview: URL.createObjectURL(imageFile),
                    })
                }, 'image/jpeg', 1);
                setinvalidImage(null)
            };
            img.onerror = () => {
                setinvalidImage('Invalid image content.');
                return false;
            };
            //debugger
            img.src = e.target.result;
        };
        reader.readAsDataURL(imageFile);
    }


    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Edit Kandidat</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/regulations" >Kandidat</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Tambah</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addDapilWilayah" className="row" noValidate>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Group Pemilihan<span className="required">*</span></label>
                                <RadioGroup
                                    row={!loadingGroup}
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    value={group.toString()}
                                    defaultValue={group.toString()}
                                    name="radio-buttons-group"
                                >
                                    {loadingGroup ? (
                                        <Skeleton variant="text" />
                                    ) : (
                                        groups.length == 0 ? (
                                            <div className='selected-empty'>Belum Ada Data</div>
                                        ) : (

                                            groups.map(row => (

                                                <FormControlLabel
                                                    name="group"
                                                    onChange={event => handleChange(event, 'group')}
                                                    value={row.id.toString()}
                                                    control={<Radio />}
                                                    label={row.name}
                                                    key={row.id} />


                                            ))
                                        )
                                    )}

                                </RadioGroup>
                                {validator.message('group', group, 'required')}
                                <div className='text-danger'>{errors.group}</div>
                            </div>

                            <div className="form-group">
                                <label>Pemilihan<span className="required">*</span></label>
                                <RadioGroup
                                    row={!loadingSubGroup}
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    value={subgroup.toString()}
                                    defaultValue={subgroup.toString()}
                                    name="radio-buttons-group"
                                >
                                    {loadingSubGroup ? (
                                        <Skeleton variant="text" />
                                    ) : (
                                        subgroups.length == 0 ? (
                                            <div className='selected-empty'>Belum Ada Data</div>
                                        ) : (

                                            subgroups.map(row => (

                                                <FormControlLabel
                                                    name="subgroup"
                                                    onChange={e => handleChange(e, 'subgroup')}
                                                    value={row.id.toString()}
                                                    control={<Radio />}
                                                    label={row.name}
                                                    key={row.id} />


                                            ))
                                        )
                                    )}

                                </RadioGroup>
                                {validator.message('subgroup', subgroup, 'required')}
                                <div className='text-danger'>{errors.subgroup}</div>
                            </div>

                            <div className="form-group">
                                <label>Produk Di Beli <span className="required"></span></label>
                                <FormGroup>
                                    <FormControlLabel
                                        control={<Checkbox checked={campaign} onChange={e => setCampaign(!campaign)} name="campaign" />}
                                        label="Kampanye"
                                    />
                                    <FormControlLabel
                                        control={<Checkbox checked={survey} onChange={e => setSurvey(!survey)} name="survey" />}
                                        label="Polling"
                                    />
                                    <FormControlLabel
                                        control={<Checkbox checked={count} onChange={e => setCount(!count)} name="count" />}
                                        label="Quick Count"
                                    />
                                </FormGroup>

                                <div className='text-danger'></div>
                            </div>

                            {(group == 2 && subgroup != 4) &&
                                <div className="form-group">
                                    <label>Partai</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_partai'
                                        name='kode_partai'
                                        label='Partai'
                                        onChange={e => setKodePartai(e.target.value)}
                                        value={kodePartai}
                                        fullWidth
                                    >
                                        {partai.map(option => (
                                            <MenuItem key={parseInt(option.id)} value={option.real_id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            }

                            <div className="form-group">
                                <label>Provinsi <span className="required">*</span></label>
                                <TextField variant='outlined'
                                    select
                                    id='kode_province'
                                    name='kode_province'
                                    label='Provinsi'
                                    onChange={e => handleChange(e, 'kode_province')}
                                    value={kodeProvince}
                                    defaultValue={''}
                                    fullWidth
                                >
                                    {provinces.map(option => (
                                        <MenuItem key={parseInt(option.id)} value={option.kode_provinsi}>
                                            {option.province}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                {validator.message('kode_province', kodeProvince, 'required')}
                                <div className='text-danger'>{errors.kode_province}</div>
                            </div>

                            {(subgroup == 2 || subgroup == 6) &&
                                <div className="form-group">
                                    <label>Kabupaten</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_kabupaten'
                                        name='kode_kabupaten'
                                        label='Kabupaten'
                                        onChange={e => setKodeKabupaten(e.target.value)}
                                        value={kodeKabupaten}
                                        fullWidth
                                    >
                                        {kabupaten.map(option => (
                                            <MenuItem key={parseInt(option.id)} value={option.kode_kabupaten}>
                                                {option.kabupaten}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            }

                            {(group == 2 && subgroup != 4) &&
                                <div className="row">
                                    <div className="col-md-8">
                                        <div className="form-group">
                                            <label>Dapil</label>
                                            <TextField variant='outlined'
                                                select
                                                id='kode_dapil'
                                                name='kode_dapil'
                                                label='Dapil'
                                                onChange={e => setKodeDapil(e.target.value)}
                                                value={kodeDapil}
                                                fullWidth
                                            >
                                                {dapil.map(option => (
                                                    <MenuItem key={parseInt(option.id)} value={option.real_id}>
                                                        {option.name}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </div>

                                    </div>
                                    <div className="col-md-4">
                                        {dapil.length == 0 &&
                                            <Button
                                                variant="contained"
                                                className="add-kabupaten mr-3"
                                                onClick={handleGoDapil}
                                            >
                                                Buat Dapil
                                            </Button>
                                        }
                                    </div>
                                </div>
                            }

                            <div className="form-group">
                                <label>Nama Kandidat <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='name'
                                    name="name"
                                    label="Input Nama Kandidat"
                                    onChange={e => setName(e.target.value)}
                                    value={name}
                                    fullWidth
                                />
                                {validator.message('name', name, 'required')}
                                <div className='text-danger'>{errors.name}</div>
                            </div>

                            <div className="form-group">
                                <label>Singkatan Nama <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='singkatan-nama-kandidat'
                                    name="singkatan"
                                    label="Input Singkatan Nama"
                                    onChange={e => setSingkatan(e.target.value)}
                                    value={singkatan}
                                    fullWidth
                                />
                                {validator.message('singkatan', singkatan, 'required')}
                                <div className='text-danger'>{errors.singkatan}</div>
                            </div>

                            <div className="form-group">
                                <label>Slogan <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='slogan-kandidat'
                                    name="slogan"
                                    label="Input Slogan Kandidat"
                                    onChange={e => setSlogan(e.target.value)}
                                    value={slogan}
                                    fullWidth
                                />
                                {validator.message('slogan', slogan, 'required')}
                                <div className='text-danger'>{errors.slogan}</div>
                            </div>


                        </div>
                        <div className="col-md-6">
                        <div className="form-group">
                                <label>Warna<span className="required">*</span></label>
                                <div className="input-warna" style={{ background: `${warna}` }}></div>
                                <RadioGroup
                                    row={!loadingGroup}
                                    aria-labelledby="demo-radio-buttons-group-label"
                                    defaultValue="female"
                                    name="radio-buttons-group"
                                >

                                    {loadingColors ? (
                                        <Skeleton variant="text" />
                                    ) : (
                                        colors.length == 0 ? (
                                            <div className='selected-empty'>Belum Ada Data</div>
                                        ) : (

                                            colors.map(option => (
                                                <div key={parseInt(option.id)} className="rounded"
                                                    style={{ background: `${option.color}` }} value={option.color}
                                                    onClick={event => handleChangeWarna(event, option.color)}>
                                                </div>
                                            ))
                                        )
                                    )}

                                </RadioGroup>
                                {validator.message('group', group, 'required')}
                                <div className='text-danger'>{errors.group}</div>
                            </div>

                            <div className='form-group'>
                                <label>Tanggal Pemilihan</label>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <DatePicker
                                        label='Tanggal Pemilihan'
                                        value={date}
                                        onChange={(event) => setDate(event)}
                                        format={'dd MMM yyyy'}
                                        cancelLabel='BATAL'
                                        inputVariant='outlined'
                                    />
                                </MuiPickersUtilsProvider>
                                {date &&
                                    <button
                                        type='button'
                                        onClick={(event) => setDate(event)}
                                        className='btn btn-delete-select'><i className='fas fa-times-circle'> </i>
                                    </button>
                                }
                                <div className='text-danger'>{errors.date}</div>
                            </div>

                            {invalidImage !== null ? <h4 className="error"> {invalidImage} </h4> : null}
                            <div className="form-group">
                                <label>Photo Kandidat</label>
                                <input type="file" className="form-control" name="image" onChange={onChangeImage} />
                            </div>

                            <div className="form-group">
                                <label>Preview</label>
                                <div className="text-center">
                                    {image.filepreview !== null ?
                                        <img className="previewimg" src={image.filepreview} alt="UploadImage" />
                                        : <img src={process.env.REACT_APP_API_STORAGE_PATH + photo} alt='photo kandidat' className="" />}
                                </div>
                            </div>
                        </div>

                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    )

}

