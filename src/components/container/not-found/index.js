import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NotFound extends Component {

    componentDidMount() {

        document.title = 'Admin - 404 Not Found';

    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name=""  noValidate>
                            <div className="text-center">
                                <h1>Halaman tidak diketemukan</h1>
                                <Link to="/" >Kembali</Link>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        )
    }
}

export default NotFound;

