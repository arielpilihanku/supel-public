import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Skeleton from '@material-ui/lab/Skeleton';

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

class DapilAdd extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            groups: [],
            provinces: [],
            tmpProvince: [],
            kode_province: '',
            nama_province: '',
            showAdd: false,
            showForm: false,

            subgroup: '',
            name: '',
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Halaman Buat Dapil';

        Api.get('/subgroup/campaigns?group=2&for=dapil').then(resp => {

            if (resp.data) {
                let groups = resp.data;

                this.setState({
                    loading: false,
                    groups: groups,
                })
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/province/areas').then(resp => {

            if (resp) {

                this.setState({
                    provinces: Object.keys(resp).map(key => resp[key]),
                })

            }

        }).catch(err => {
            console.log(err);
        });

    }

    handleChangeProvince(e, prop) {

        this.setState({

            tmpProvince: e.target.value,
            kode_province: e.target.value.kode_provinsi,
            nama_province: e.target.value.province,
            openKabupaten: true

        });

    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };

    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleSubmit = (e) => {

        e.preventDefault();

        if (!validator.allValid()) {

            this.setState({
                errors: validator.getErrorMessages()
            }
            );

            return false;

        }

        this.setState({
            errors: {},
            loadingButton: true,
        });

        let formData = new FormData();
        formData.append('subgroup', this.state.subgroup);
        formData.append('kode_provinsi', this.state.kode_province);
        formData.append('nama_provinsi', this.state.nama_province);
        formData.append('name', this.state.name);

        Api.putFile('/dapil/campaigns', {
            method: 'POST',
            body: formData
        }).then(resp => {

            const dapil = resp.data;

            this.setState({
                loadingButton: false,
            });

            history.push('/dapil-wilayah/add/' + dapil.id);

            this.showMessage(true, 'Dapil berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                this.setState({
                    errors: err.errors,
                    loadingButton: false
                }
                );

                this.showMessage(false, 'Invalid format data');
            }
        });

    };

    handleGo = (link) => {
        history.push(link);
    };


    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Buat Dapil</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item"><Link to="/dapil" >Dapil</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Buat</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name="add" id="addUser" className="row" noValidate>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Pemilihan<span className="required">*</span></label>
                                    <RadioGroup
                                        row={!this.state.loading}
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue="female"
                                        name="radio-buttons-group"
                                    >
                                        {this.state.loading ? (
                                            <Skeleton variant="text" />
                                        ) : (
                                            this.state.groups.length === 0 ? (
                                                <div className='selected-empty'>Belum Ada Data</div>
                                            ) : (

                                                this.state.groups.map(row => (

                                                    <FormControlLabel 
                                                        name="subgroup"
                                                        onChange={(e) => this.handleChange(e, 'subgroup')}
                                                        value={row.kode} 
                                                        control={<Radio />} 
                                                        label={row.name} 
                                                        key={row.id} />


                                                ))
                                            )
                                        )}

                                    </RadioGroup>
                            
                                </div>
                            </div>
                            <div className="col-md-6"></div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Provinsi <span className="required">*</span></label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_province'
                                        name='kode_province'
                                        label='Provinsi'
                                        onChange={(e) => this.handleChangeProvince(e, 'kode_province')}
                                        value={this.state.tmpProvince || ''}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {this.state.provinces.map(option => (
                                            <MenuItem key={parseInt(option.id)} value={option}>
                                                {option.province}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    {validator.message('kode_province', this.state.kode_province, 'required')}
                                    <div className='text-danger'>{this.state.errors.kode_province}</div>
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Nama Dapil <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='name'
                                        name="name"
                                        label="Input Nama Dapil"
                                        onChange={(e) => this.handleChange(e, 'name')}
                                        value={this.state.name}
                                        fullWidth
                                    />
                                    {validator.message('name', this.state.name, 'required')}
                                    <div className='text-danger'>{this.state.errors.name}</div>
                                </div>
                            </div>
                            <div className="col-md-6"></div>


                            <div className="col-12 text-left">
                                <Button
                                    variant="contained"
                                    className="mr-3"
                                    onClick={() => this.handleGo('/dapil')}
                                >
                                    Kembali
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={this.handleSubmit}
                                    disabled={this.state.loadingButton}
                                >
                                    Simpan{this.state.loadingButton && <i className="fa fa-spinner fa-spin"> </i>}
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default DapilAdd;
