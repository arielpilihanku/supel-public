import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import MenuItem from '@material-ui/core/MenuItem';

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

class DapilEdit extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingArrayKabupaten: true,
            loadingArrayKecamatan: true,
            loadingButton: false,
            loadingKabupaten: false,
            openKabupaten: false,
            openKecamatan: false,
            errors: {},

            id: '',
            kode_province: '',
            nama_province: '',
            kode_kabupaten: '',
            nama_kabupaten: '',
            kode_kecamatan: '',
            subgroup: '',
            name: '',

            provinces: [],
            kabupaten: [],
            kecamatan: [],
            tmpKabupaten: [],
            arrayKabupaten: [],
            arrayKodeKabupaten: [],
            tmpKecamatan: [],
            arrayKecamatan: [],
            arrayKodeKecamatan: [],
            rows: [],
            sendPageNumber: '',
            sendPerPage: '',
            total: 0,
            perPage: 10,
            currentPage: 1,
            currentPageTable: 0,
            totalData: 0,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Halaman Rubah Partai';

        const id = this.props.match.params.id;

        Api.get('/dapil/campaigns/' + id).then(resp => {

            if (resp.data) {
                let data = resp.data;
                this.setState({
                    id: data.id,
                    subgroup: data.subgroup,
                    name: data.name,
                    kode_province: data.kode_provinsi,
                    nama_province: data.nama_provinsi,
                });
                this.__fetchDataKabupaten(data.kode_provinsi);
            }

        }).catch(err => {
            console.log(err);
        });

        Api.get('/dapil-wilayah/campaigns/' + id).then(resp => {

            if (resp.data) {
                let data = resp.data;
                this.setState({
                    rows: data
                });

            }

        }).catch(err => {
            console.log(err);
        });



    }

    __fetchDataKabupaten = province => {

        Api.get('/kabupaten/areas?province=' + province).then(resp => {


            if (resp) {

                this.setState({
                    kabupaten: Object.keys(resp).map(key => resp[key])
                })

            }

        }).catch(err => {
            console.log(err);
        });
    };

    __fetchDataKecamatan = kecamatan => {

        Api.get('/kecamatan/areas?kabupaten=' + kecamatan).then(resp => {


            if (resp) {

                this.setState({
                    kecamatan: Object.keys(resp).map(key => resp[key])
                })

            }

        }).catch(err => {
            console.log(err);
        });
    };

    handleChangeProvince(e, prop) {

        this.setState({

            kode_province: e.target.value,
            openKabupaten: true

        });

        this.__fetchDataKabupaten(e.target.value);

    };

    handleChangeKabupaten(e, prop) {

        this.setState({

            tmpKabupaten: e.target.value,
            kode_kabupaten: e.target.value.kode_kabupaten,
            openKecamatan: true & this.state.subgroup === 'DPRK'

        });

        this.__fetchDataKecamatan(e.target.value.kode_kabupaten);

    };

    handleChangeKecamatan(e, prop) {

        this.setState({

            tmpKecamatan: e.target.value,
            kode_kecamatan: e.target.value.kode_kecamatan,

        });


    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };


    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleSubmit = (e) => {

        e.preventDefault();

        if (!validator.allValid()) {

            this.setState({
                errors: validator.getErrorMessages()
            }
            );

            return false;

        }

        this.setState({
            errors: {},
            loadingButton: true,
        }
        );

        const params = {
            'wilayah': this.state.rows
        };

        Api.patch('/dapil-wilayah/campaigns/' + this.state.id, params).then(resp => {

            this.setState({
                loadingButton: false,
            });

            history.push('/dapil');

            this.showMessage(true, 'Berhasil merubah dapil.');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                this.setState({
                    errors: err.errors,
                    loadingButton: false
                });

                this.showMessage(false, 'Invalid format data');
            }
        });

    };


    handleGo = (link) => {
        history.push(link);
    };

    handleAddArrayKabupaten = () => {
        const kabupaten = this.state.tmpKabupaten;
        const check = this.state.rows.filter(function (row) {
            return row.kode_kabupaten === kabupaten.kode_kabupaten
        });

        if (check.length === 1) {
            this.showMessage(false, 'Data kabupaten sudah ada');
        } else {
            this.setState({
                rows: [...this.state.rows, kabupaten],
            })
        }

    };

    handleDeleteArrayKabupaten(e, kabupaten) {

        e.preventDefault();

        this.setState({
            arrayKabupaten: this.state.arrayKabupaten.filter(function (arrayKabupaten) {
                return arrayKabupaten !== kabupaten
            }),
            arrayKodeKabupaten: this.state.arrayKodeKabupaten.filter(function (kodeKabupaten) {
                return kodeKabupaten !== kabupaten.kode_kabupaten
            }),
        });

    };

    handleAddArrayKecamatan = () => {
        const kecamatan = this.state.tmpKecamatan;
        const check = this.state.rows.filter(function (row) {
            return row.kode_kecamatan === kecamatan.kode_kecamatan
        });

        if (check.length === 1) {
            this.showMessage(false, 'Data kecamatan sudah ada');
        } else {
            this.setState({
                rows: [...this.state.rows, kecamatan],
            })
        }

    };

    handleDeleteArrayKecamatan(e, kecamatan) {

        e.preventDefault();

        this.setState({
            arrayKecamatan: this.state.arrayKecamatan.filter(function (arrayKecamatan) {
                return arrayKecamatan !== kecamatan
            }),
            arrayKodeKecamatan: this.state.arrayKodeKecamatan.filter(function (kodeKecamatan) {
                return kodeKecamatan !== kecamatan.kode_kecamatan
            }),
        });

    };

    handleDeleteRows(e, dataDelete) {

        e.preventDefault();

        this.setState({
            rows: this.state.rows.filter(function (row) {
                return row !== dataDelete
            }),
        });

    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Rubah Dapil</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item"><Link to="/regulations" >Dapil</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Rubah</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name="add" id="addUser" className="row" noValidate>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Pemilihan</label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='subgroup'
                                        name="subgroup"
                                        label="Pemilihan"
                                        value={this.state.subgroup}
                                        disabled={true}
                                        fullWidth
                                    />
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <TextField variant="outlined"
                                        type='text'
                                        id='nama_province'
                                        name="nama_province"
                                        label="Provinsi"
                                        onChange={(e) => this.handleChange(e, 'nama_province')}
                                        value={this.state.nama_province}
                                        disabled={true}
                                        fullWidth
                                    />
                                    {validator.message('nama_province', this.state.nama_province, 'required')}
                                    <div className='text-danger'>{this.state.errors.nama_province}</div>
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Nama Dapil <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='name'
                                        name="name"
                                        label="Nama Dapil"
                                        onChange={(e) => this.handleChange(e, 'name')}
                                        value={this.state.name}
                                        fullWidth
                                    />
                                    {validator.message('name', this.state.name, 'required')}
                                    <div className='text-danger'>{this.state.errors.name}</div>
                                </div>
                            </div>
                            <div className="col-md-6">

                            </div>

                            <div className="col-md-6">

                                <div className="form-group">
                                    <label>Kabupaten</label>
                                    <TextField variant='outlined'
                                        select
                                        id='kode_kabupaten'
                                        name='kode_kabupaten'
                                        label='Kabupaten'
                                        onChange={(e) => this.handleChangeKabupaten(e, 'kode_kabupaten')}
                                        value={this.state.tmpKabupaten || ''}
                                        defaultValue={''}
                                        fullWidth
                                    >
                                        {this.state.kabupaten.map(option => (
                                            <MenuItem key={parseInt(option.id)} value={option}>
                                                {option.kabupaten}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    {validator.message('kode_kabupaten', this.state.kode_kabupaten, 'required')}
                                    <div className='text-danger'>{this.state.errors.kode_kabupaten}</div>
                                </div>

                            </div>
                            <div className="col-md-6">
                                {this.state.subgroup !== 'DPRK' ? (

                                    <Button
                                        variant="contained"
                                        className="add-kabupaten mr-3"
                                        onClick={() => this.handleAddArrayKabupaten()}
                                    >
                                        ADD
                                    </Button>) : ''}
                            </div>

                            {this.state.subgroup === 'DPRK' ? (
                                <>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label>Kecamatan</label>
                                            <TextField variant='outlined'
                                                select
                                                id='kode_kecamatan'
                                                name='kode_kecamatan'
                                                label='Kecamatan'
                                                onChange={(e) => this.handleChangeKecamatan(e, 'kode_kecamatan')}
                                                value={this.state.tmpKecamatan}
                                                fullWidth
                                            >
                                                {this.state.kecamatan.map(option => (
                                                    <MenuItem key={parseInt(option.id)} value={option}>
                                                        {option.kecamatan}
                                                    </MenuItem>
                                                ))}
                                            </TextField>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <Button
                                            variant="contained"
                                            className="add-kabupaten mr-3"
                                            onClick={() => this.handleAddArrayKecamatan()}
                                        >
                                            ADD
                                        </Button>
                                    </div>
                                </>
                            ) : ''
                            }

                            <div className="col-12 table-kabupaten">
                                <div className="">
                                    <Table className="table-list" size="small" >
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Kode</TableCell>
                                                <TableCell>Kabupaten</TableCell>
                                                {this.state.subgroup === 'DPRK' && <TableCell>Kode Kecamatan</TableCell>}
                                                {this.state.subgroup === 'DPRK' && <TableCell>Nama Kecamatan</TableCell>}
                                                <TableCell></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>

                                            {this.state.rows.length == 0 ? (
                                                <TableRow>
                                                    <TableCell colSpan={2} style={{ textAlign: "left" }}>Belum ada dapil wilayah dipilih</TableCell>
                                                </TableRow>
                                            ) : (
                                                this.state.rows.map(row => (
                                                    <TableRow
                                                        key={row.id}
                                                        selected={row.selected}>
                                                        <TableCell>
                                                            <span>{row.kode_kabupaten == null ? '' : row.kode_kabupaten}</span>
                                                        </TableCell>
                                                        <TableCell>
                                                            <span>{row.kabupaten == null ? '' : row.kabupaten}</span>
                                                        </TableCell>
                                                        {this.state.subgroup === 'DPRK' &&
                                                            <TableCell>
                                                                <span>{row.kode_kecamatan == null ? '' : row.kode_kecamatan}</span>
                                                            </TableCell>
                                                        }
                                                        {this.state.subgroup === 'DPRK' &&
                                                            <TableCell>
                                                                <span>{row.kecamatan == null ? '' : row.kecamatan}</span>
                                                            </TableCell>
                                                        }

                                                        <TableCell>
                                                            <button className="btn-icon" onClick={(e) => this.handleDeleteRows(e, row)}>
                                                                <i className="fas fa-trash-alt"> </i>
                                                            </button>
                                                        </TableCell>

                                                    </TableRow>
                                                ))
                                            )}
                                        </TableBody>
                                    </Table>
                                </div>
                            </div>

                            <div className="col-12 text-left">
                                <Button
                                    variant="contained"
                                    className="mr-3"
                                    onClick={() => this.handleGo('/dapil')}
                                >
                                    Kembali
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={this.handleSubmit}
                                    disabled={this.state.loadingButton}
                                >
                                    Simpan{this.state.loadingButton && <i className="fa fa-spinner fa-spin"> </i>}
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default DapilEdit;
