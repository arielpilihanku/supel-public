import React,{ Component } from 'react';
import { Link } from 'react-router-dom';
import {bindActionCreators} from "redux";
import {RootActions} from "../../../shared/root-action";
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {connect} from "react-redux";
import {history} from "../../../shared/configure-store";
import moment from "moment/moment";

class Navbar extends Component {

    constructor(props) {

        super(props);

        this.handleToggleSidebar = this.handleToggleSidebar.bind(this);
    }

    handleToggleSidebar(){

        if(this.props.toggle_sidebar_state){

            this.props.setHideSidebar();

        }else{

            this.props.setShowSidebar();

        }

    }

    handleGo  = (link) => {

        history.push(link);
    };

    render() {
        const date = Date.now();
        return(

            <header className="header-nav ">
                <nav className="navbar justify-content-xl-end align-items-center">
                    <Link to="#" className="navbar-brand m-0 p-0 d-xl-none">
                       
                    </Link>
                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        className="d-xl-none"
                        onClick={this.handleToggleSidebar}
                    >
                        <MenuIcon />
                    </IconButton>
                    <div className="date d-none d-xl-block">
                        <p className="mb-0 mr-3">{moment(date).format('DD MMM YYYY, dddd')}</p>
                    </div>
                </nav>
            </header>

        )

    }

}

const mapStateToProps = (state) => {

    return {

        toggle_sidebar_state: state.toggle_sidebar_state,

        profile_state: state.profile_state

    };

};

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators(RootActions, dispatch)

};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);