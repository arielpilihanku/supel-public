import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MUIEditor, { MUIEditorState } from "react-mui-draft-wysiwyg";

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

class ArticleAdd extends Component {
    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            campaign: [],
            showAdd: false,
            showForm: false,

            campaign_id: '',
            title: '',
            type: '',
            category: '',
            editorState: MUIEditorState.createEmpty(),
            tags: '',
            img: '',
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Dashboard Buat Artikel';

        Api.get('/campaigns').then(resp => {

            if (resp.data) {
                let campaign = resp.data;

                this.setState({
                    loading: false,
                    campaign: campaign,
                })
            }

        }).catch(err => {
            console.log(err);
        });

    }

    handleChangeArticle(e) {
        this.setState({
            editorState: e
        });
    }

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };

    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleSubmit = (e) => {

        e.preventDefault();

        if (!validator.allValid()) {

            this.setState({
                errors: validator.getErrorMessages()
            }
            );

            return false;

        }

        this.setState({
            errors: {},
            loadingButton: true,
        });

        let formData = new FormData();
        formData.append('name', this.state.name);

        Api.putFile('/regulationgroups', {
            method: 'POST',
            body: formData
        }).then(resp => {

            this.setState({
                loadingButton: false,
            });

            history.push('/categories');

            this.showMessage(true, 'Kategori berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                this.setState({
                    errors: err.errors,
                    loadingButton: false
                }
                );

                this.showMessage(false, 'Invalid format data');
            }
        });

    };

    handleGo = (link) => {
        history.push(link);
    };


    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Buat Artikel</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item"><Link to="/categories" >Artikel</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Buat</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <form name="add" id="addUser" className="row" noValidate>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Nama Kandidat </label>
                                    <TextField variant='outlined'
                                        select
                                        id='campaign_id'
                                        name='campaign_id'
                                        label='Kandidat'
                                        onChange={(e) => this.handleChange(e, 'campaign_id')}
                                        value={this.state.campaign_id}
                                        fullWidth
                                    >
                                        {this.state.campaign.map(option => (
                                            <MenuItem key={parseInt(option.real_id)} value={option.name}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Type<span className="required">*</span></label>
                                    <RadioGroup
                                        row={!this.state.loading}
                                        aria-labelledby="radio-button-type"
                                        defaultValue="public"
                                        name="radio-buttons-type-article"
                                    >
                                        <FormControlLabel
                                            name="type"
                                            onChange={(e) => this.handleChange(e, 'type')}
                                            value="public"
                                            control={<Radio />}
                                            label="Publik" />
                                        <FormControlLabel
                                            name="type"
                                            onChange={(e) => this.handleChange(e, 'type')}
                                            value="campaign"
                                            control={<Radio />}
                                            label="Kandidat" />
                                    </RadioGroup>
                                    {validator.message('type', this.state.type, 'required')}
                                    <div className='text-danger'>{this.state.errors.type}</div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Title <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='title'
                                        name="title"
                                        label="Title"
                                        onChange={(e) => this.handleChange(e, 'title')}
                                        value={this.state.title}
                                        fullWidth
                                    />
                                    {validator.message('name', this.state.title, 'required')}
                                    <div className='text-danger'>{this.state.errors.title}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Kategori <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='category'
                                        name="category"
                                        label="Kategori"
                                        onChange={(e) => this.handleChange(e, 'category')}
                                        value={this.state.category}
                                        fullWidth
                                    />
                                    {validator.message('category', this.state.category, 'required')}
                                    <div className='text-danger'>{this.state.errors.category}</div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Artikel <span className="required">*</span></label>
                                    <MUIEditor editorState={this.state.editorState} onChange={this.handleChangeArticle} />
                                    {validator.message('article_text', this.state.article_text, 'required')}
                                    <div className='text-danger'>{this.state.errors.article_text}</div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label>Gambar <span className="required">*</span></label>
                                    <TextField variant="outlined"
                                        type='text'
                                        id='category'
                                        name="category"
                                        label="Kategori"
                                        onChange={(e) => this.handleChange(e, 'category')}
                                        value={this.state.category}
                                        fullWidth
                                    />
                                    {validator.message('category', this.state.category, 'required')}
                                    <div className='text-danger'>{this.state.errors.category}</div>
                                </div>
                            </div>


                            <div className="col-12 text-left">
                                <Button
                                    variant="contained"
                                    className="mr-3"
                                    onClick={() => this.handleGo('/categories')}
                                >
                                    Kembali
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    className=""
                                    onClick={this.handleSubmit}
                                    disabled={this.state.loadingButton}
                                >
                                    Simpan{this.state.loadingButton && <i className="fa fa-spinner fa-spin"> </i>}
                                </Button>
                            </div>
                        </form>
                    </div>
                </div>

                <ToastContainer />

            </div>
        )
    }
}

export default ArticleAdd;
