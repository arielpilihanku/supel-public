import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Link } from 'react-router-dom';
import SimpleReactValidator from "simple-react-validator";
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import ErrorHandling from "../../../libraries/error-handling";
import { toast, ToastContainer } from "react-toastify";
import { history } from "../../../shared/configure-store";
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MUIEditor, {
    MUIEditorState,
    toolbarControlTypes // Added
} from "react-mui-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import { convertToRaw } from 'draft-js';

require('dotenv').config();
const validator = new SimpleReactValidator({ locale: process.env.REACT_APP_LOCALE });

export default function ArticleAdd() {

    const config = {
        editor: {
            style: {
                minHeight: '600px',
            },
        },
    }

    const [editorState, setEditorState] = React.useState(
        MUIEditorState.createEmpty()
    );

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [campaign, setCampaign] = React.useState('');
    const [type, setType] = React.useState('public');
    const [title, setTitle] = React.useState('');
    const [category, setCategory] = React.useState('');
    const [tags, setTags] = React.useState('');
    const [dataCampaign, setDataCampaign] = React.useState([]);
    const [userInfo, setuserInfo] = React.useState({
        file: [],
        filepreview: null,
    });

    const [invalidImage, setinvalidImage] = React.useState(null);
    let reader = new FileReader();

    const onChange = (newState) => {
        setEditorState(newState);
    };

    React.useEffect(() => {

        Api.get('/campaigns').then(resp => {

            if (resp.data) {
                const campaign = resp.data;

                setDataCampaign(campaign);
            }

        }).catch(err => {
            console.log(err);
        });

    }, []);

    const handleBack = () => {
        history.push('/articles');
    };

    const handleSubmit = async () => {

        if (!validator.allValid()) {

            setErrors(validator.getErrorMessages());
    
            return false;

        }

        const rawContent = convertToRaw(editorState.getCurrentContent());
        const markup = draftToHtml(rawContent);

        const formdata = new FormData()
        formdata.append('campaign_id', campaign);
        formdata.append('title', title);
        formdata.append('type', type);
        formdata.append('category', category);
        formdata.append('tags', tags);
        formdata.append('article_text', markup);
        formdata.append('img', userInfo.file);

        Api.putFile('/articles', {
            method: 'POST',
            body: formdata
        }).then(resp => {

            setLoading(false);

            history.push('/articles');

           showMessage(true, 'Kategori berhasil dibuat');

        }).catch(err => {

            if (ErrorHandling.checkErrorTokenExpired(err)) {

            } else {

                setErrors(err.errors);
                setLoading(false);

                showMessage(false, 'Invalid format data');
            }
        });
    }

    const showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    const onChangeImage = (event) => {
        const imageFile = event.target.files[0];
        const imageFileName = event.target.files[0].name;

        if (!imageFile) {
            setinvalidImage('Please select image.');
            return false;
        }

        if (!imageFile.name.match(/\.(jpg|jpeg|png|JPG|JPEG|PNG|gif)$/)) {
            setinvalidImage('Please select valid image JPG,JPEG,PNG');
            return false;
        }
        reader.onload = (e) => {
            const img = new Image();
            img.onload = () => {

                //------------- Resize img code ----------------------------------
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 437;
                var MAX_HEIGHT = 437;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
                ctx.canvas.toBlob((blob) => {
                    const file = new File([blob], imageFileName, {
                        type: 'image/jpeg',
                        lastModified: Date.now()
                    });
                    setuserInfo({
                        ...userInfo,
                        file: file,
                        filepreview: URL.createObjectURL(imageFile),
                    })
                }, 'image/jpeg', 1);
                setinvalidImage(null)
            };
            img.onerror = () => {
                setinvalidImage('Invalid image content.');
                return false;
            };
            //debugger
            img.src = e.target.result;
        };
        reader.readAsDataURL(imageFile);
    }

    return (
        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h1 className="page-title">Buat Artikel</h1>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                        <li className="breadcrumb-item"><Link to="/categories" >Artikel</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Buat</li>
                    </ol>
                </nav>

            </div>
            <div className="col-12 mt-3 px-lg-5">
                <div className="table-wrapper">
                    <form name="add" id="addUser" className="row" noValidate>

                        <div className="col-md-8">
                            <div className="form-group">
                                <label>Nama Kandidat </label>
                                <TextField variant='outlined'
                                    select
                                    id='campaign_id'
                                    name='campaign_id'
                                    label='Kandidat'
                                    onChange={e => setCampaign(e.target.value)}
                                    value={campaign}
                                    fullWidth
                                >
                                    {dataCampaign.map(option => (
                                        <MenuItem key={parseInt(option.real_id)} value={option.real_id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label>Type<span className="required">*</span></label>
                                <RadioGroup
                                    row={loading}
                                    aria-labelledby="radio-button-type"
                                    defaultValue="public"
                                    name="radio-buttons-type-article"
                                >
                                    <FormControlLabel
                                        name="type"
                                        onChange={e => setType(e.target.value)}
                                        value="public"
                                        control={<Radio />}
                                        label="Publik" />
                                    <FormControlLabel
                                        name="type"
                                        onChange={e => setType(e.target.value)}
                                        value="campaign"
                                        control={<Radio />}
                                        label="Kandidat" />
                                </RadioGroup>
                            </div>
                        </div>

                        <div className="col-md-8">
                            <div className="form-group">
                                <label>Title <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='title'
                                    name="title"
                                    label="Title"
                                    onChange={e => setTitle(e.target.value)}
                                    value={title}
                                    fullWidth
                                />
                                {validator.message('title', title, 'required')}
                                <div className='text-danger'>{errors.title}</div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label>Kategori <span className="required">*</span></label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='category'
                                    name="category"
                                    label="Kategori"
                                    onChange={e => setCategory(e.target.value)}
                                    value={category}
                                    fullWidth
                                />
                                {validator.message('category', category, 'required')}
                                <div className='text-danger'>{errors.category}</div>
                            </div>
                        </div>

                        <div className="col-md-8">
                            <div className="form-group">
                                <label>Tags</label>
                                <TextField variant="outlined"
                                    type='text'
                                    id='tags'
                                    name="tags"
                                    label="Tags"
                                    onChange={e => setTags(e.target.value)}
                                    value={tags}
                                    fullWidth
                                />
                            </div>
                        </div>
                        <div className="col-md-4">
                            {invalidImage !== null ? <h4 className="error"> {invalidImage} </h4> : null}
                            <div className="form-group">
                                <label>Gambar</label>
                                <input type="file" className="form-control" name="upload_file" onChange={onChangeImage} />
                            </div>
                        </div>

                        <div className="col-md-8">
                            <div className="form-group">
                                <label>Artikel <span className="required">*</span></label>
                                <MUIEditor
                                    editorStyle={{ minHeight: "700px" }}
                                    editorState={editorState}
                                    onChange={onChange}
                                    config={config}
                                />
                                {validator.message('editorState', editorState, 'required')}
                                <div className='text-danger'>{errors.editorState}</div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <label>Preview</label>
                                {userInfo.filepreview !== null ?
                                    <img className="previewimg" src={userInfo.filepreview} alt="UploadImage" />
                                    : null}
                            </div>
                        </div>


                        <div className="col-12 text-left">
                            <Button
                                variant="contained"
                                className="mr-3"
                                onClick={handleBack}
                            >
                                Kembali
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className=""
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Simpan{loading && <i className="fa fa-spinner fa-spin"> </i>}
                            </Button>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer />

        </div>
    );

}
