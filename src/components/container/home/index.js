import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import { connect } from "react-redux";
import { withRouter, Link } from 'react-router-dom';
import { bindActionCreators } from "redux";
import { RootActions } from "../../../shared/root-action";

import Api from "../../../libraries/api";

function Home(props) {

    const [loading, setLoading] = React.useState(false);
    const [errors, setErrors] = React.useState([]);
    const [status, setStatus] = React.useState([]);
    const [date, setDate] = React.useState('0');
    const [total, setTotal] = React.useState('0');
    const [isAdmin, setIsAdmin] = React.useState(false);

    React.useEffect(() => {
   
        fetchDashboardAdmin();

    }, []);

    const fetchDashboardAdmin = async () => {

        console.log(props.role_state)

        setLoading(true);

        setIsAdmin(true);

        Api.get('/dashboards').then(resp => {

            if (resp) {
                const data = resp;

                setStatus(data.status);
                setTotal(data.total);
                setDate(data.date);

                setLoading(false)
            }

        }).catch(err => {
            setLoading(false)
            console.log(err);
        });
    };



    return (

        <div className="row main-content">
            <div className="col-12 px-lg-5">
                <h2 className="page-title">Halaman Utama Admin</h2>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                    </ol>
                </nav>


                {props.role_state !== 'superadmin' && <div className="row mt-4">

                    <div className='col-md-2'>
                        <Link to="/capel" target='_blank'>
                            <div className="card-info white">
                                <div className="row align-items-center">
                                    <div className="col-8 col-md-10 col-xl-8">
                                        <label className="label-grey">Total</label>
                                        <p className="number">{total}</p>
                                        <p>Calon Pemilih</p>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </div>

                    {loading ? (
                        <div><CircularProgress color="primary" /></div>
                    ) : (
                        (status.length === 0) ? (
                            <ListItem>
                                <ListItemIcon>
                                    <NotInterestedIcon />
                                </ListItemIcon>
                                <ListItemText
                                    primary="Tidak Ada Data"
                                    secondary=""
                                />
                            </ListItem>
                        ) : (
                            status.map(option => (
                                <div className='col-md-3'>
                                    <Link to="/capel?filter=pasti" target='_blank'>
                                        <div className="card-info white">
                                            <div className="row align-items-center">
                                                <div className="col-8 col-md-10 col-xl-8">
                                                    <label className="label-grey">{option.status}</label>
                                                    <p className="number">{option.percentage}</p>
                                                    <p>Dari Total Calon Pemilih</p>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            )))
                    )}

                    <div className='col-md-2'>
                        <Link to="/capel?filter=pasti" target='_blank'>
                            <div className="card-info white">
                                <div className="row align-items-center">
                                    <div className="col-8 col-md-10 col-xl-8">
                                        <label className="label-grey">Hari Pemilihan</label>
                                        <p className="number">{date}</p>
                                        <p>Hari Lagi</p>
                                    </div>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>}

            </div>

        </div>

    )

}

const mapStateToProps = (state) => {

    return {

        toggle_sidebar_state: state.toggle_sidebar_state,

        profile_state: state.profile_state,

        permission_state: state.permission_state,

        role_state: state.role_state,
    };

};

const mapDispatchToProps = (dispatch) => {

    return bindActionCreators(RootActions, dispatch)

};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));