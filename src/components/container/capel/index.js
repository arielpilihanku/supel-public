import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Api from "../../../libraries/api";
import Button from '@material-ui/core/Button';
import { toast, ToastContainer } from "react-toastify";
import TablePagination from "@material-ui/core/TablePagination";
import TablePaginationActions from "../../presentational/table-pagination-actions";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import { history } from "../../../shared/configure-store";
import MenuItem from '@material-ui/core/MenuItem';

require('dotenv').config();

class Capel extends Component {
    _isMounted = false;

    constructor(props) {

        super(props);

        this.state = {
            loading: true,
            loadingButton: false,
            errors: {},

            orderBy: '',
            sortedBy: '',
            searchBy: '',

            roles: [],
            showDialog: false,
            idDelete: '',
            currDelete: '',
            kode_kabupaten: '',
            kode_kecamatan: '',
            kode_desa: '',

            rows: [],
            provinsi: [],
            kabupaten: [],
            kecamatan: [],
            desaAll: [],
            desaFilter: [],
            total: 0,
            perPage: 10,
            currentPage: 1,
            currentPageTable: 0,

            isEdit: false,
            isDelete: false,
            isCreate: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {

        document.title = 'Dashboard Calon Pemilih';

        this.__fetchData(false);

    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    __fetchData = update => {
        
        this.setState({
            loading: true
        });

        this._isMounted = true;

        let page = update ? parseInt(this.state.currentPage + 1) : this.state.currentPage;

        let sort = this.state.orderBy ? '&orderBy=' + this.state.orderBy + '&sortedBy=' + this.state.sortedBy : '';
        let search = this.state.searchBy ? '&search=' + this.state.searchBy : '';

        let route = '';

        if (search) {
            route = '/voters?limit=' + this.state.perPage + sort + search;
        } else {
            route = '/voters?limit=' + this.state.perPage + '&page=' + page + sort + search;
        }

        Api.get(route).then(resp => {
            if (this._isMounted) {

                const aggregate = resp.meta.custom.aggregate;

                if (resp.data) {

                    this.setState({
                        rows: resp.data,
                        perPage: resp.meta.pagination.per_page,
                        currentPage: resp.meta.pagination.current_page,
                        currentPageTable: resp.meta.pagination.current_page - 1,
                        total: resp.meta.pagination.total,
                        loading: false,
                        kabupaten: aggregate.kabupaten,
                        kecamatan: aggregate.kecamatan,
                        desaAll: aggregate.desa,
                        desaFilter: aggregate.desa,
                    })

                }
            }
        }).catch(err => {
            console.log(err);
        });
    };

    handleSearch = () => {
        this.__fetchData(false);
    };
    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.__fetchData(false);
        }
    };

    handleSort = (val) => {

        let sortedBy = this.state.sortedBy;

        if (val !== this.state.orderBy) {
            sortedBy = 'asc';
        } else {
            if (sortedBy && sortedBy === 'asc') {
                sortedBy = 'desc';
            } else {
                sortedBy = 'asc';
            }
        }

        this.setState({
            orderBy: val,
            sortedBy: sortedBy,
            loading: true
        }, () => {
            this.__fetchData(false);
        });
    };

    handleChange(e, prop) {

        this.setState({

            [prop]: e.target.value

        })

    };

    handleOpen = (row) => {
        this.setState({
            showDialog: true,
            idDelete: row.id,
            currDelete: row.name,
        })
    };

    handleClose = () => {
        this.setState({
            showDialog: false,
        })
    };

    handleDelete = () => {

        if (this.state.idDelete) {
            Api.delete('/regulationgroups/' + this.state.idDelete, '', false).then(resp => {

                this.setState({
                    showDialog: false,
                }
                );

                this.showMessage(true, 'User successfully deleted');
                this.__fetchData(false);

            }).catch(err => {

                this.setState({
                    showDialog: false
                }
                );

                this.showMessage(true, 'User successfully deleted');
                this.__fetchData(false);
            });
        }
    };

    showMessage = (status, message) => {
        if (status) {
            toast.success(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        } else {
            toast.error(message, {
                position: toast.POSITION.BOTTOM_RIGHT
            });
        }
    };

    handleChangePage = (event, newPage) => {
        this.setState({
            rows: [],
            currentPage: newPage,
            currentPageTable: newPage,
            loading: true,
        }, () => {
            this.__fetchData(true);
        });
    };

    handleChangeRowsPerPage = event => {
        this.setState({
            perPage: parseInt(event.target.value, 10),
            currentPage: 1,
            loading: true,
        }, () => {
            this.__fetchData(false);
        });
    };

    handleGo = (link) => {

        history.push(link);
    };

    handleChangeKabupaten(e, prop) {

        this.setState({
            kode_kabupaten: e.target.value,
        });

    };

    handleChangeKecamatan(e, prop) {

        const { value } = e.target;

        this.setState({
            desaFilter: this.state.desaAll.filter(function (data) {
                return data.kode_kecamatan === value;
            })
        });

        this.setState({
            kode_kecamatan: e.target.value,
        });

    };

    handleChangeDesa(e, prop) {

        this.setState({
            kode_desa: e.target.value,
        });

    };

    handleFilter = event => {

        this.setState({
            loading: true
        });

        this._isMounted = true;

        let page = this.state.currentPage;

        let sort = this.state.orderBy ? '&orderBy=' + this.state.orderBy + '&sortedBy=' + this.state.sortedBy : '';
        let search = this.state.searchBy ? '&search=' + this.state.searchBy : '';
        let filter = '&kabupaten=' + this.state.kode_kabupaten + '&kecamatan=' + this.state.kode_kecamatan + '&desa=' + this.state.kode_desa;

        let route = '';

        if (search) {
            route = '/voters?limit=' + this.state.perPage + sort + search + filter;
        } else {
            route = '/voters?limit=' + this.state.perPage + '&page=' + page + sort + search + filter;
        }

        Api.get(route).then(resp => {
            if (this._isMounted) {

                const aggregate = resp.meta.custom.aggregate;

                if (resp.data) {

                    this.setState({
                        rows: resp.data,
                        perPage: resp.meta.pagination.per_page,
                        currentPage: resp.meta.pagination.current_page,
                        currentPageTable: resp.meta.pagination.current_page - 1,
                        total: resp.meta.pagination.total,
                        loading: false,
                        kabupaten: aggregate.kabupaten,
                        kecamatan: aggregate.kecamatan,
                        desa: aggregate.desa
                    })

                }
            }
        }).catch(err => {
            console.log(err);
        });

    };

    render() {
        return (
            <div className="row main-content">
                <div className="col-12 px-lg-5">
                    <h1 className="page-title">Calon Pemilih</h1>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/" >Home</Link></li>
                            <li className="breadcrumb-item active" aria-current="page">Calon Pemilih</li>
                        </ol>
                    </nav>

                </div>
                <div className="col-12 mt-3 px-lg-5">
                    <div className="table-wrapper">
                        <div className="row align-items-center mb-md-3">
                            <div className="col-md-6">
                                <TextField
                                    id="input-with-icon-textfield"
                                    variant="outlined"
                                    className="search-field"
                                    onChange={(e) => this.handleChange(e, 'searchBy')}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleSearch}
                                    placeholder="Cari disini"
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="start">
                                                <IconButton
                                                    aria-label="Search click"
                                                    onClick={this.handleSearch}
                                                >
                                                    <i className="fas fa-search"> </i>
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </div>
                            <div className="col-md-6 text-md-right">
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    className="mb-3 mb-md-0"
                                    onClick={this.handleFilter}
                                >
                                    Filter
                                </Button>

                            </div>
                        </div>

                        <Table className="table-list" size="small" >
                            <TableHead>
                                <TableRow>
                                    <TableCell className="clickable" onClick={() => this.handleSort('provinsi')} >Provinsi
                                        {this.state.orderBy === 'provinsi' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('kabupaten')} >Kabupaten
                                        {this.state.orderBy === 'kabupaten' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('kecamatan')} >Kecamatan
                                        {this.state.orderBy === 'kecamatan' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('desa')} >Desa
                                        {this.state.orderBy === 'desa' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('nkk')} >NKK
                                        {this.state.orderBy === 'nkk' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('nik')} >NIK
                                        {this.state.orderBy === 'nik' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell className="clickable" onClick={() => this.handleSort('name')} >Nama
                                        {this.state.orderBy === 'name' && (
                                            <span className="icon-sort">{
                                                this.state.sortedBy === 'asc' ? (
                                                    <i className="fas fa-sort-up"> </i>
                                                ) : (
                                                    <i className="fas fa-sort-down"> </i>
                                                )
                                            }</span>
                                        )}
                                    </TableCell>
                                    <TableCell>Tempat Lahir</TableCell>
                                    <TableCell>Tanggal Lahir</TableCell>
                                    <TableCell>Kawin</TableCell>
                                    <TableCell>Jenis Kelamin</TableCell>
                                    <TableCell>Alamat</TableCell>
                                    <TableCell>RT</TableCell>
                                    <TableCell>RW</TableCell>
                                    <TableCell>Difabel</TableCell>
                                    <TableCell>TPS</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell></TableCell>
                                    <TableCell>
                                        <TextField variant='outlined'
                                            select
                                            id='kode_kabupaten'
                                            name='kode_kabupaten'
                                            label='Kabupaten'
                                            onChange={(e) => this.handleChangeKabupaten(e, 'kode_kabupaten')}
                                            value={this.state.kode_kabupaten}
                                            fullWidth
                                        >
                                            {this.state.kabupaten.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode}>
                                                    {option.nama}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </TableCell>
                                    <TableCell>
                                        <TextField variant='outlined'
                                            select
                                            id='kode_kecamatan'
                                            name='kode_kecamatan'
                                            label='Kecamatan'
                                            onChange={(e) => this.handleChangeKecamatan(e, 'kode_kecamatan')}
                                            value={this.state.kode_kecamatan}
                                            fullWidth
                                        >
                                            {this.state.kecamatan.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode}>
                                                    {option.nama}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </TableCell>
                                    <TableCell>
                                        <TextField variant='outlined'
                                            select
                                            id='kode_desa'
                                            name='kode_desa'
                                            label='Desa'
                                            onChange={(e) => this.handleChangeDesa(e, 'kode_desa')}
                                            value={this.state.kode_desa}
                                            fullWidth
                                        >
                                            {this.state.desaFilter.map(option => (
                                                <MenuItem key={parseInt(option.id)} value={option.kode}>
                                                    {option.nama}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                                {this.state.loading ? (
                                    <TableRow>
                                        <TableCell colSpan={16} align="center" className="py-5">
                                            <CircularProgress color="primary" />
                                        </TableCell>
                                    </TableRow>
                                ) : (
                                    this.state.rows.length === 0 ? (
                                        <TableRow style={{ height: 33 * this.state.perPage }}>
                                            <TableCell colSpan={16} style={{ textAlign: "center" }}>Belum ada calon pemilih disimpan</TableCell>
                                        </TableRow>
                                    ) : (
                                        this.state.rows.map(row => (
                                            <TableRow
                                                key={row.id}
                                                selected={row.selected}>
                                                <TableCell>
                                                    <span>{row.nama_provinsi === null ? '' : row.nama_provinsi}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nama_kabupaten === null ? '' : row.nama_kabupaten}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nama_kecamatan === null ? '' : row.nama_kecamatan}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nama_desa === null ? '' : row.nama_desa}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nkk === null ? '' : row.nkk}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.nik === null ? '' : row.nik}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.name === null ? '' : row.name}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.tempat_lahir === null ? '' : row.tempat_lahir}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.tanggal_lahir === null ? '' : row.tanggal_lahir}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.kawin === null ? '' : row.kawin}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.jenis_kelamin === null ? '' : row.jenis_kelamin}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.alamat === null ? '' : row.alamat}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.rt === null ? '' : row.rt}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.rw === null ? '' : row.rw}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.difabel === null ? '' : row.difabel}</span>
                                                </TableCell>
                                                <TableCell>
                                                    <span>{row.tps === null ? '' : row.tps}</span>
                                                </TableCell>
                                            </TableRow>
                                        ))
                                    )
                                )}

                            </TableBody>
                        </Table>
                    </div>

                    <TablePagination
                        rowsPerPageOptions={[10, 25, 50]}
                        component="div"
                        count={this.state.total}
                        rowsPerPage={this.state.perPage}
                        page={this.state.currentPageTable}
                        backIconButtonProps={{
                            'aria-label': 'previous page',
                        }}
                        nextIconButtonProps={{
                            'aria-label': 'next page',
                        }}
                        onChangePage={this.handleChangePage}
                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                        ActionsComponent={TablePaginationActions}
                    />

                </div>

                <Dialog
                    maxWidth='sm'
                    fullWidth={true}
                    open={this.state.showDialog}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">DELETE KATEGORI: {this.state.currDelete}</DialogTitle>
                    <DialogContent>
                        <p>Kamu yakin mau menghapus kategori ini?</p>
                    </DialogContent>
                    <DialogActions>
                        <button className="btn btn-blue-trans" onClick={this.handleClose}>
                            Batalkan
                        </button>
                        <button className="btn btn-blue" onClick={this.handleDelete}>
                            Hapus
                        </button>
                    </DialogActions>
                </Dialog>

                <ToastContainer />

            </div>
        )
    }
}

export default Capel;